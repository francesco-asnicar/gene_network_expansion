#include <iostream> // cout
#include <cstdlib> // exit
#include "sched_util.h" // check_stop_daemons, count_unsent_results
#include "sched_msgs.h" // log_messages
#include "str_util.h"
#include "svn_version.h"
#include "boinc_db.h"
#include "error_numbers.h"
#include "backend_lib.h"
#include "parse.h"
#include "util.h"
#include "sched_config.h"

using namespace std;

/**
 *
 */
int main(void) {
	int n;
	int retval;

	// check the BOINC config file
	retval = config.parse_file("..");

	if (retval) {
		log_messages.printf(MSG_CRITICAL, "can't parse config.xml: %s\n", boincerror(retval));
		return retval;
	}

	// check the DB connection
	retval = boinc_db.open(config.db_name, config.db_host, config.db_user, config.db_passwd);

	if (retval) {
		log_messages.printf(MSG_CRITICAL, "can't open db: %s\n", boincerror(retval));
		return retval;
	}

	// check if the daemon can run
	check_stop_daemons();

	// check unsent jobs
	retval = count_unsent_results(n, 0);

	if (retval) {
		log_messages.printf(MSG_CRITICAL, "can't retrieve the unsent wus: %s\n", boincerror(retval));
		return retval;
	}

	cout << n;

	return 0;
}
