#!/usr/bin/php
<?php
define('BOINC_OWNER', 'boincadm');

# parameters
$res_path = '/home/boincadm/projects/test/gene_results/';
$dst_host = 'boincadm.ssh@www.iaoa.org';
#$dst_host = 'boincadm.ssh@www.loa.istc.cnr.it';
$dst_path = 'drop';
$mail_to = 'valter@science.unitn.it';
$mail_from = 'boincadm@gene.disi.unitn.it';

# logging
require_once 'KLogger.php';
$log = new KLogger('php://stdout', KLogger::DEBUG);

# command line arguments
$opt_exec = false;
$opt_mail = false;
$opt_one = false;
getopt_parse();
if(!$opt_exec) $log->LogInfo(basename(__FILE__, '.php').' (DEBUG mode)');

# error handling
set_error_handler('error_handler');

# check user
if($opt_exec) {
  $user = trim(shell_exec('whoami'));
  if($user <> BOINC_OWNER) {
    $log->LogFatal('Must run as \''.BOINC_OWNER.'\''); 
    exit;
  }
}

register_shutdown_function('shutdown');

# locking (single instance)
$fp_lock = fopen('/tmp/gene_move', 'w+');
if(!flock($fp_lock, LOCK_EX | LOCK_NB)) {
  $log->LogFatal('Another instance is running'); 
  exit;
}

# signal handling
$signal = false;
declare(ticks = 10);
pcntl_signal(SIGINT, "signal_handler");
function signal_handler($sig) {
  global $signal, $log;
  switch($sig) {
    case SIGINT:
      $log->LogWarn("Got SIGINT");
      $signal = true;
  }
}

# shutdown
function shutdown() {
  global $fp_lock, $mail_to, $mail_from, $log; 
  global $opt_mail, $opt_exec;

  if($opt_mail && $opt_exec) {
  mail($mail_to, 'gene_move report ('.date("Y-m-d H:i:s").')', $log->buffer,
     "From: $mail_from\r\n" .
     "X-Mailer: PHP/" . phpversion() );
  }

  fclose($fp_lock);
}

# move pcim
function move_pcim($name, $number_wus) {
  global $src_path, $dst_host, $dst_path, $log;
  global $opt_exec;
  $return_var = 0;

  $ssh = 'ssh -o ConnectTimeout=5 '.$dst_host. ' ';
  $dir = "$dst_path/$name/results";

  $command = $ssh."\"mkdir -p $dir\""; # join mkdir & tar?
  if($opt_exec) exec($command, $output, $return_var);
  else echo $command."\n";
  if($return_var != 0) {
    $log->LogFatal('Exec ssh (mkdir) error: '.$return_var);
    return $return_var;
  }
  $command = '(cd '.$name.';tar --transform=\'flags=r;s|$|.gz|\' -cpf - *)'." | $ssh \"tar -xpf - -C $dir\"";
  #$command = 'tar -C '.$name.' --transform=\'flags=r;s|[0-9]$|$&.gz|\' -cpf - .'." | $ssh \"tar -xpf - -C $dir\"";
  if($opt_exec) exec($command, $output, $return_var);
  else echo $command."\n";
  if($return_var != 0) {
    $log->LogFatal('Exec ssh (tar) error: '.$return_var);
    return $return_var;
  }

  return 0;
}

# open mysql connection to gene db
$mysql = new mysqli("localhost", "ugene", "db@G3ne", "gene");
if($mysql->connect_errno) {
  $log->LogFatal("Failed to connect to MySQL: $mysql->connect_errno ($mysql->connect_error)"); 
  exit;
}

#$sqlquery = 'SELECT executions_path, results_path FROM `wg_params`;';
#$result = $mysql->query($sqlquery);
#$params = $result->fetch_object();

# get needed pcim from db
$sqlquery = 'SELECT pcim_id,organism,number_wus FROM `pcim` WHERE in_execution = 1;';
$result_pcim = $mysql->query($sqlquery);
if($mysql->errno) {
  $log->LogFatal("MySQL: (pcim) $mysql->error"); 
  exit;
}
if($mysql->affected_rows == 0) {
  $log->LogInfo('No pc-im in execution.');
}

#--------------------
$starttime = microtime(true);
if(!chdir($res_path)) {
  $log->LogFatal('Cannot change dir to '.$res_path);
  exit;
}
#--------------------

# browse pcim
$total_files = 0;
$n = 0; $nw = 0;
while($row = $result_pcim->fetch_object()) {
  if($signal) {
    $log->LogWarn('Got SIGINT (stopping)....');
    exit;
  }
  
# look for missing wus
  if($row->number_wus > 0) {
    $number_wus = $row->number_wus;
    $name = $row->pcim_id.'_'.$row->organism;
#---------------------
# get results
    $dir = $name;
    if(is_dir($dir)) {
      chdir($dir);
      $sub_list = glob('*', GLOB_NOSORT);
      list($id) = explode('_', $dir);
      $wu_done[$id] = count($sub_list);
      $total_files += $wu_done[$id];
      chdir('..');
      #echo $res_path.$dir,': ', $wu_done[$id],"\n";
    }
#---------------------

    $found = 0;
    if(isset($wu_done[$row->pcim_id])) {
      $found = $wu_done[$row->pcim_id];
    }
    $missing = $number_wus - $found;
#---------------------------------------------------
    if($missing == 0) {  
      if($n > 0 && $opt_one) break;
      $n++; 
      $nw += $number_wus;
# check results consistency
      $num = array();
      foreach($sub_list as $f) {
        if(preg_match('/wu-(\d+)_/', $f, $m)) $num[] = $m[1];
      }
      $num = array_unique($num);
      if(max($num) != $number_wus || count($num) != $number_wus) {
        $log->LogWarn('Numeric inconsistency with '.$name.' results');
        continue;
      }
# move results
      $log->LogInfo('Moving '.$name.' ('. $number_wus.' workunits)');
      if(move_pcim($name, $number_wus) != 0) {
        exit;
      }
# update db
      $sqlquery = "UPDATE pcim SET in_execution='0',executed='1' WHERE pcim_id=$row->pcim_id;";
      if($opt_exec) $result = $mysql->query($sqlquery);
      else echo $sqlquery."\n";
      if($mysql->errno) {
        $log->LogFatal("MySQL: (pcim) $mysql->error");
        exit;
      }
# remove files
      if($opt_exec) {
        chdir($name);
        $remove_status = array_map('unlink', $sub_list);
        if(count(array_filter($remove_status)) != count($sub_list)) {
          $log->LogWarn('Cannot remove '.$name.'/*');
        }
        chdir('..');
        rmdir($name);
      } else {
        echo 'Removing '.getcwd().'/'.$name."\n";
      }
    }
#---------------------------------------------------
    if($missing < 0) {
      $log->LogWarn('Check '.$name.' ('. $number_wus.' workunits) ... missing < 0');
    }

  } else {
    echo "Error: (number_wus == 0)\n";
  }
}
$log->LogInfo("Total PC-IMs moved: $n ($nw workunits)");

# close mysql connection
$mysql->close();

$endtime = microtime(true);
$timediff = $endtime - $starttime;
$log->LogInfo('Running time '.  sprintf("%.2f", $timediff).  " seconds ($total_files files)");

#echo memory_get_usage() . "\n";

#---------------------------------
function error_handler($errno, $errstr, $errfile, $errline) {
  global $log;
  if($errno == E_NOTICE) {
    $log->LogFatal('PHP '.$errstr.' at line '.$errline.' ('.$errfile.')');
    exit;
  } else {
    return false;
  }
}

#---------------------------------
function getopt_parse() {
  global $opt_exec, $opt_mail, $opt_one, $log;

  require_once 'Console/Getopt.php';
  $getopt = new Console_Getopt;
  $opts = $getopt->getopt($getopt->readPHPArgv(),'', array('exec', 'mail', 'one'));
  unset($getopt);

  if(PEAR::isError($opts)) {
    $log->LogError($opts->getMessage());
    exit;
  }
  foreach($opts[0] as $o) {
    switch($o[0]) {
      case '--exec':
        $opt_exec = true;
        break;
      case '--mail':
        $opt_mail = true;
        break;
      case '--one':
        $opt_one = true;
        break;
    }
  }
}
?>
