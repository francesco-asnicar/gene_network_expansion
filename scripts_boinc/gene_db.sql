-- create db if not exists
CREATE DATABASE IF NOT EXISTS gene;

-- use the db
USE gene;

-- create a user with restricted privileges
CREATE USER 'ugene'@'localhost' IDENTIFIED BY 'db@G3ne';
REVOKE ALL PRIVILEGES ON *.* FROM 'ugene'@'localhost';
GRANT ALL ON gene.* TO 'ugene'@'localhost';

-- table 'pcim' which represents one PC-IM execution
DROP TABLE IF EXISTS pcim;
CREATE TABLE pcim (
	pcim_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, -- unique id
	organism CHAR(2) NOT NULL, -- two carachters represent the organism, e.g Hs Homo sapiens
	pcim_name VARCHAR(256) NOT NULL, -- the pc-im textual name
	lgn_path VARCHAR(256) NOT NULL, -- absolute path to lgn file in the server
	alpha DECIMAL(4,3) NOT NULL, -- alpha value for pc run
	iterations INT UNSIGNED NOT NULL, -- number of entire turns of the genome
	tile_size INT UNSIGNED NOT NULL, -- size of the graph of the pc, length of the tile rows
	npc INT UNSIGNED DEFAULT 0 NOT NULL, -- specific number of PC per work unit, if zero the default value in wg_params
	priority TINYINT UNSIGNED DEFAULT 1 NOT NULL, -- priority range from 1 to 10, where 1 is the lowest and 10 is the highest
	time_creation TIMESTAMP DEFAULT 0, -- creation time of the pc-im
	time_update TIMESTAMP DEFAULT NOW() ON UPDATE NOW(), -- last update time pc-im
	number_wus INT UNSIGNED DEFAULT 0 NOT NULL, -- will contains the number of work-units generated for the pc-im
	executed BOOLEAN DEFAULT 0 NOT NULL, -- 0 if the pc-im is not executed and still to execute, 1 otherwise
	error BOOLEAN DEFAULT 0 NOT NULL, -- 1 if the pc-im has something wrong and has generated error
	in_execution BOOLEAN DEFAULT 0 NOT NULL, -- 1 when the work-units are executing, 0 when all its work-units are executed
	email_sent BOOLEAN DEFAULT 0 NOT NULL, -- 1 when the e-mail is sent to the user that submitted the pcim, 0 otherwise
	pre_alarm TINYINT UNSIGNED DEFAULT 95 NOT NULL, -- set a treshold when reached the move_results.py daemon sent a pre-alarm e-mail (range from 1 to 100)
	email_pre_alarm BOOLEAN DEFAULT 0 NOT NULL, -- 1 when the e-mail was sent when pre_alarm reached, 0 otherwise

	PRIMARY KEY (pcim_id),

	INDEX (priority),
	INDEX (executed),
	INDEX (error),
	INDEX (in_execution),
	INDEX (email_sent),
	INDEX (email_pre_alarm)
) ENGINE=InnoDB;

DELIMITER $$

-- check the value of alpha, priority and pre_alarm during INSERT
CREATE TRIGGER chk_pcim_bi BEFORE INSERT ON pcim
FOR EACH ROW
BEGIN
	DECLARE baddata INT;

	-- check alpha
	SET baddata = 0;
	IF NEW.alpha <= 0.000 THEN
		SET baddata = 1;
	END IF;
	IF NEW.alpha > 1.000 THEN
		SET baddata = 1;
	END IF;
	IF baddata = 1 THEN
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Cannot insert the tuple because alpha is out of the valid range (0.000, 1.000]';
	END IF;

	-- check priority
	IF NEW.priority < 1 THEN
		SET NEW.priority = 1;
	END IF;
	IF NEW.priority > 10 THEN
		SET NEW.priority = 1;
	END IF;

	-- check pre_alarm
	IF NEW.pre_alarm < 1 THEN
		SET NEW.pre_alarm = 95;
	END IF;
	IF NEW.pre_alarm > 100 THEN
		SET NEW.pre_alarm = 95;
	END IF;
END; $$

-- check the value of alpha, priority and pre_alarm during UPDATE
CREATE TRIGGER chk_pcim_bu BEFORE UPDATE ON pcim
FOR EACH ROW
BEGIN
	DECLARE baddata INT;

	-- check alpha
	SET baddata = 0;
	IF NEW.alpha <= 0.000 THEN
		SET baddata = 1;
	END IF;
	IF NEW.alpha > 1.000 THEN
		SET baddata = 1;
	END IF;
	IF baddata = 1 THEN
		SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Cannot update the tuple because alpha is out of the valid range (0.000, 1.000]';
	END IF;

	-- check priority
	IF NEW.priority < 1 THEN
		SET NEW.priority = 1;
	END IF;
	IF NEW.priority > 10 THEN
		SET NEW.priority = 1;
	END IF;

	-- check pre_alarm
	IF NEW.pre_alarm < 1 THEN
		SET NEW.pre_alarm = 95;
	END IF;
	IF NEW.pre_alarm > 100 THEN
		SET NEW.pre_alarm = 95;
	END IF;
END; $$

DELIMITER ;

-- -- table 'user' represent the users that can submit PC-IM executions
-- DROP TABLE IF EXISTS user;
-- CREATE TABLE user (
--  user_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, -- unique id
--  first_name VARCHAR(256) NOT NULL, -- the first name of the user
--  second_name VARCHAR(256) NOT NULL, -- the second name of the user
--  title VARCHAR(256) NOT NULL, -- the title of the user (e.g., "Dott.", "Prof.", "PhD", etc.)
--  e_mail VARCHAR(256) NOT NULL, -- the e-mail address of the user
--  affiliation VARCHAR(256) NOT NULL, -- the affiliation of the user (e.g., "UniTn", "F.E.M", "CNR", "CoSBi", etc.)

--  PRIMARY KEY (user_id)
-- ) ENGINE=InnoDB;

-- -- 'user_pcim' link a user with a PC-IM execution
-- DROP TABLE IF EXISTS user_pcim;
-- CREATE TABLE user_pcim (
--  user_id BIGINT UNSIGNED NOT NULL, -- user identificator on table user
--  pcim_id BIGINT UNSIGNED NOT NULL, -- pc-im identificator on table pcim

--  PRIMARY KEY (user_id, pcim_id),
--  FOREIGN KEY (user_id) REFERENCES user(user_id) ON DELETE CASCADE,
--  FOREIGN KEY (pcim_id) REFERENCES pcim(pcim_id) ON DELETE CASCADE
-- ) ENGINE=InnoDB;

-- table 'experiments' which store the input file (one experiment per file)
DROP TABLE IF EXISTS experiments;
CREATE TABLE experiments (
	exp_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, -- unique id
	exp_name VARCHAR(256) NOT NULL, -- name of the experiment file
	exp_path VARCHAR(256) NOT NULL, -- absolute path of the experiment file in the server

	PRIMARY KEY (exp_id)
) ENGINE=InnoDB;

-- 'pcim_experiments' link the input files with a PC-IM execution
DROP TABLE IF EXISTS pcim_experiments;
CREATE TABLE pcim_experiments (
	pcim_id BIGINT UNSIGNED NOT NULL, -- pc-im identificator on table pcim
	exp_id BIGINT UNSIGNED NOT NULL, -- experiment identificator on table experiments

	PRIMARY KEY (pcim_id, exp_id),
	FOREIGN KEY (pcim_id) REFERENCES pcim(pcim_id) ON DELETE CASCADE,
	FOREIGN KEY (exp_id) REFERENCES experiments(exp_id) ON DELETE CASCADE
) ENGINE=InnoDB;

-- table 'wg_params' store just one record, that is the work-generator parameters
DROP TABLE IF EXISTS wg_params;
CREATE TABLE wg_params (
	application_name VARCHAR(256) NOT NULL, -- name of the application to give to the work units
	cushion INT UNSIGNED NOT NULL, -- maximum number of work units in the queue
	replication_factor TINYINT UNSIGNED NOT NULL DEFAULT 2, -- number of people that have to validate a single work-unit
	max_time_sleep INT UNSIGNED NOT NULL, -- maximum time that the work-generator have to sleep in the exponential backoff
	num_pc_wu INT UNSIGNED NOT NULL, -- number of pc runs for work-unit
	deadline TINYINT UNSIGNED NOT NULL DEFAULT 5, -- deadline of the work-unit in days
	out_template VARCHAR(256) NOT NULL, -- relative path to the output template, rooted at the project directory
	executions_path VARCHAR(256) NOT NULL, -- absolute path used to put all files of a pc-im
	results_path VARCHAR(256) NOT NULL -- absolute path used to put all results
) ENGINE=InnoDB;

-- default parameters for the work-generator
INSERT INTO wg_params VALUES ("gene", 50, 2, 1800, 500, 5, "templates/gene_result.xml", "/home/boincadm/projects/test/gene_execs");

-- view that restrict the fields visible during the insertion of a new PC-IM. The hidden fields are used by the software
DROP VIEW IF EXISTS insert_pcim;
CREATE VIEW insert_pcim AS
	SELECT  organism, pcim_name, lgn_path, alpha, iterations, tile_size, npc, priority
	FROM pcim

-- view that return the PC-IM id along with the pre_alarm treshold only for the PC-IMs not already pre-alarmed
DROP VIEW IF EXISTS pre_alarm;
CREATE VIEW pre_alarm AS
	SELECT pcim_id, pre_alarm
	FROM pcim
	WHERE email_pre_alarm = 0 and email_sent = 0;

-- view that return the PC-IM to execute
DROP VIEW IF EXISTS pcim_in_execution;
CREATE VIEW pcim_in_execution AS
	SELECT pcim_id, pcim_name, organism, number_wus
	FROM pcim
	WHERE in_execution = 1;

-- view that return the PC-IM to execute
DROP VIEW IF EXISTS pcim_to_execute;
CREATE VIEW pcim_to_execute AS
	SELECT pcim_id, organism, pcim_name, lgn_path, alpha, iterations, tile_size, npc, priority
	FROM pcim
	WHERE executed = 0 and error = 0 and in_execution = 0
	ORDER BY priority DESC, pcim_id ASC;

-- view that return the PC-IM executed
DROP VIEW IF EXISTS pcim_executed;
CREATE VIEW pcim_executed AS
	SELECT pcim_id, organism, pcim_name, lgn_path, alpha, iterations, tile_size, priority
	FROM pcim
	WHERE executed = 1;

-- view that return the PC-IM that gives some errors, hence not executed
DROP VIEW IF EXISTS pcim_errors;
CREATE VIEW pcim_errors AS
	SELECT pcim_id, organism, pcim_name, lgn_path, alpha, iterations, tile_size, priority
	FROM pcim
	WHERE error = 1;
