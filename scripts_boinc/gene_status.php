<?php
require_once("../inc/util.inc");
require_once("../inc/boinc_db.inc");

$days = 45;
$res_path = '/data/boinc/gene_results/';
$task_move = 'gene_move';

$organism = array ( 'At' => 'Arabidopsis Thaliana');

function db_count($mysql, $where) {
  $sqlquery = 'SELECT 1 FROM `pcim` WHERE '.$where.';';
# echo $sqlquery.'<br>';
  if($result = $mysql->query($sqlquery)) {
    $count = $result->num_rows;
    $result->close();
  }
  return($count); 
}

# open mysql connection to gene db
$mysql = new mysqli("localhost", "ugene", "db@G3ne", "gene");
if ($mysql->connect_errno) {
  die("Failed to connect to MySQL: $mysql->connect_errno ($mysql->connect_error\n"); 
}
#echo 'Connection: '.$mysql->host_info."\n";
$time_interval = "(time_update > NOW() - INTERVAL $days DAY) ";

$count_total = db_count($mysql, $time_interval);
$count_queued = db_count($mysql, $time_interval . 'AND number_wus = 0 AND error <> 1');
$count_errors = db_count($mysql, $time_interval . 'AND error <> 0');
$count_executed = db_count($mysql, $time_interval . 'AND executed = 1');
$count_exec = $count_total - $count_queued - $count_errors - $count_executed;

$count_mono = db_count($mysql, "pcim_name LIKE '%ecm%';"); 
$count_mono_done = db_count($mysql, "pcim_name LIKE '%ecm%' AND executed =1;"); 
$count_mono_done_last = db_count($mysql, "pcim_name LIKE '%ecm%' AND executed =1 
  AND (time_update > NOW() - INTERVAL 10 DAY);"); 
$count_mono_done_last = sprintf('%.2f', $count_mono_done_last / 10.0);
$count_mono_per = sprintf('%.2f',($count_mono_done * 100) / 3343);
# get needed pcim 
$sqlquery = "SELECT * FROM `pcim` WHERE error <> 1 AND (in_execution = 1 OR number_wus = 0 OR executed = 0);";
$result_pcim = $mysql->query($sqlquery);

# close mysql connection
$mysql->close();

page_head(tra("gene@home PC-IM status"));
?>

<link rel="stylesheet" type="text/css" href="jquery/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="jquery/DataTables-1.10.3/extensions/TableTools/css/dataTables.tableTools.min.css">
<style type="text/css">
  td { font-size: 11px; }
  th.col-id, td.col-id { color: blue; text-align: right; }
  th.col-center, td.col-center { text-align: center; }
  th.col-right, td.col-right { text-align: right; }
</style>
<script type="text/javascript" language="javascript" src="jquery/jquery-1.11.1.min.js"></script>
<script type="text/javascript" language="javascript" src="jquery/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="jquery/num-mix.js"></script>
<script type="text/javascript" language="javascript" src="jquery/DataTables-1.10.3/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function() {
    $('body').css('cursor', 'auto');
    $('#pcim').dataTable( {
         "dom": 'T<"clear">lfrtip',
         "tableTools": {
            "sSwfPath": "jquery/DataTables-1.10.3/extensions/TableTools/swf/copy_csv_xls.swf",
            "aButtons": [
                "copy", "print", "csv"
            ]
         },
         stateSave: true,
         columnDefs: [ { type: 'num-mix', targets: [6, 7, 9] } ],
         "aaSorting": [[ 0, "asc" ]], 
         "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
         "iDisplayLength": 25, "autoWidth": false,
         "aoColumns": [
            { "sWidth": "40px", "sClass": "col-id" },
            { "sWidth": "30px", "sClass": "col-center"},
            { "sWidth": "140px", "sClass": "col-left"},
            { "sWidth": "140px", "sClass": "col-right"},
            { "sWidth": "40px", "sClass": "col-right"},
            { "sWidth": "40px", "sClass": "col-right"},
            { "sWidth": "30px", "sClass": "col-right"},
            { "sWidth": "30px", "sClass": "col-center"},
            { "sWidth": "40px", "sClass": "col-right"},
            { "sWidth": "30px", "sClass": "col-right"},
            { "sWidth": "60px", "sClass": "col-id"}
          ]
       }
    );
    $("#pcim_wrapper").css("width","900px");
    $("#pcim").css("width","100%");
  } );
</script>

<?php
# info (summary) table
echo '
<table style="float:left;" id="pcim_s" class="table table-bordered">
<thead>
  <tr><th colspan="5">Summary (including last '.$days.' days)</th></tr>
  <tr><th>Total</th><th>Executed</th><th>In execution</th><th>Errors</th><th>Queued</th></tr>
</thead>';
echo "
<tbody>
  <tr valign=top align=middle><td>$count_total</td><td>$count_executed</td><td>$count_exec</td><td>$count_errors</td><td>$count_queued</td></tr>
</tbody>
</table>";

echo '
<table id="pcim_m" class="table table-bordered">
<thead>
  <tr><th colspan="4">E-coli \'mono\' experiment (ecm)</th></tr>
  <tr align="middle"><th>Total</th><th>DB</th><th>Executed</th><th>Last 10 days</th></tr>
</thead>';
echo '
<tbody>
  <tr valign="top" align="middle"><td>3343</td><td>'.$count_mono.'</td><td>'.$count_mono_done.
    ' ('.$count_mono_per.'%)</td><td>'.$count_mono_done_last.'/day</td></tr>
</tbody>
</table>';

/*
# open mysql connection to "test" (boinc) db
$mysql = new mysqli("localhost", "ugene", "db@G3ne", "test");
if ($mysql->connect_errno) {
  die("Failed to connect to MySQL: $mysql->connect_errno ($mysql->connect_error\n"); 
}
*/

# check if results are moving
$warn = '';
exec("pgrep -lf $task_move", $output);
if(count($output) > 1) {
 $warn = "<br><small>\"$task_move\" is running (\"Wait for\" column below may be inaccurate)</small>";
}
#exec("pgrep -lf scp",$output);
#print_r($output);

function array_first(&$item1) {
  $item1 = substr($item1, 0, 5);
}

#5556_Ec_ecm-b0611-rna_wu-100_1481048623690
# get results (all returned)
$starttime1 = microtime(true);
if(!chdir($res_path)) die ('Cannot change dir to '.$res_path);
$dir_list = glob('*', GLOB_NOSORT);
$total_files = 0;
foreach($dir_list as $dir) {
  chdir($dir);
  $sub_list = glob('*', GLOB_NOSORT);
  list($id) = explode('_', $dir);
  $wu_done[$id] = count($sub_list);
  $total_files += $wu_done[$id];
  #echo $dir,': ', $wu_done[$id],"\n";
  chdir('..');
}
$endtime1 = microtime(true);
$starttime2 = microtime(true);
#array_walk($res_list, 'array_first');

# in execution
echo '
<table id="pcim" class="display compact">
<thead><tr><th colspan="11">PC-IM now in execution (or queued)'.$warn.'</th></tr>
<tr>
  <th>id</th><th>org</th><th align="left">lgn</th>
  <th align="left">Last update</th><th>alpha</th><th>t-size</th><th>iter</th><th>pri</th><th>nPC</th><th>nWUs</th><th>Wait</th>
</tr>
</thead><tbody>';

# browse pcim
ob_start();
$n = 0; $nw = 0;
while($row = $result_pcim->fetch_object()) {
  //$lgn = basename($row->lgn_path, '.txt');
  $lgn = pathinfo($row->lgn_path, PATHINFO_FILENAME);
  echo '<tr>';
  echo "<td>$row->pcim_id</td>";
  echo "<td>$row->organism</td>";
  echo "<td>$lgn</td>";
  echo "<td>$row->time_update</td>";
  echo "<td>$row->alpha</td>";
  echo "<td>$row->tile_size</td>";
  echo "<td>$row->iterations</td>";
  echo "<td>$row->priority</td>";
  echo "<td>$row->npc</td>";

/*
# first wu (to change)
  if($row->number_wus > 0 ) {
    $len = strlen($row->pcim_id);
    $sqlquery = "SELECT id FROM `workunit` WHERE LEFT(name, $len) = $row->pcim_id LIMIT 1";
    $result = $mysql->query($sqlquery);
    $row_w = $result->fetch_object();
    echo '<td align="right">'.$row_w->id.'</td>';
  } else {
    echo '<td></td>';
  }
}
*/
 
# missing wus
  if($row->number_wus > 0) {
    if($row->in_execution == 1) {
      echo "<td align=\"right\">$row->number_wus</td>";
      $found = 0;
      if(isset($wu_done[$row->pcim_id])) {
        $found = $wu_done[$row->pcim_id];
      }
      $missing = $row->number_wus - $found;
      if($missing == 0) {
        $n++; 
        $nw += $row->number_wus;
        $missing = '<b>move</b>';
      }
      echo '<td>'.$missing.'</td>';
    } else {
      echo '<td>?</td><td>reissue</td>';
    }
  } else {
    echo '<td>?</td><td>start</td>';
  }
  echo "</tr>\n";
}
ob_end_flush();
echo "</tbody></table>\n";
echo "PC-IMs waiting to be moved: $n ($nw results)<p>\n";

$endtime2 = microtime(true);
$timediff1 = $endtime1 - $starttime1;
$timediff2 = $endtime2 - $starttime2;
$df1 = disk_free_space("/");
$dt1 = disk_total_space("/");
$dp1 = sprintf('%.2f',($df1 / $dt1) * 100);
$df2 = disk_free_space("/data");
$dt2 = disk_total_space("/data");
$dp2 = sprintf('%.2f',($df2 / $dt2) * 100);

echo '<small>Loading time '; 
printf("%.2f+", $timediff1); 
printf("%.2f", $timediff2); 
echo ' seconds, '.$total_files." files, disk($dp1%, $dp2%) free (".date("Y-m-d H:i:s e").")</small>\n";
#

#$mysql->close();
#
page_tail();
?>
