from itertools import chain


def readBoincFrequency(path, separator='\t'):
    frequencies = []

    with open(path, 'r') as f:
        for line in f:
            if line.strip():
                count, node = line.strip().split(separator)
                frequencies.append((int(node), int(count)))

    return dict(frequencies)


def readBoincParameters(path, separator=': '):
    parameters = []
    with open(path, 'r') as f:
        for line in f:
            if line != "":
                parameters.append(line.split(separator)[1].replace('\n', ''))

        return parameters


def readBoincResults(path, edgeCount, separator='\t', whitelist=None):
    with open(path) as f:
        for line in f:
            if line and (not line.startswith('#')):
                edge, val = tuple(map(lambda s: s.strip(), line.split(separator)))
                x, y = edge.lower().split(',')

                #                if ('csge' in x) or ('csge' in y):
                #                    print x, y

                if not whitelist or (x in whitelist or y in whitelist):
                    edge = (min(x, y), max(x, y))

                    #                    if 'csge' in edge:
                    #                        print x, y, edge

                    if edge in edgeCount:
                        edgeCount[edge] += int(val)
                    else:
                        edgeCount[edge] = int(val)


def boincPost(resultPath, parameterPath, frequencyPath, lgnPath, expressionPath, classesPath, outPath, verbose=False):
    '''
    Given the following input paths
    -   result folder
    -   the parameter file
    -   the frequencies file
    -   lgn file
    -   expression data file
    -   FEM classes file
    writes the file at 'outPath' with the following format:
    1)  An header with the description of the runs
    2)  A table containing, for each edge related with the lgn, the absolute and relative frequency,
        if it's internal and the FEM class (if provided).

    '''
    import math
    import parser
    import utilities
    from glob import glob

    lgn = parser.readLGNFile(lgnPath)
    # print "lgn", lgn
    # lgnNodes = set()
    # for x, y in lgn:
    #    lgnNodes.add(x)
    #    lgnNodes.add(y)

    lgnNodes = set(list(chain.from_iterable(lgn)))
    index2id = parser.readProbeIds(expressionPath)

    # reading params
    parameters = readBoincParameters(parameterPath)
    # print "parameters", parameters
    header = parameters[0]
    # print "header", header
    iterations = int(parameters[3])
    # print "iterations", iterations
    tileSize = int(parameters[4])
    # print "tileSize", tileSize
    surplus = readBoincFrequency(frequencyPath)
    # print "surplus", surplus
    tileNumber = int(math.ceil((len(index2id) - len(lgnNodes)) / float(tileSize - len(lgnNodes))))
    # print "tileNumber", tileNumber

    #   with open('prova', 'w') as f:
    #       for i in index2id:
    #           f.write(str(i) + ' ' + str(index2id[i]) + '\n')

    # overwhelminingly complicated node count
    nodeCount = {}
    for i, probe in index2id.iteritems():
        # probe = index2id[i]
        if probe in lgnNodes:
            nodeCount[probe] = iterations * tileNumber
        else:
            nodeCount[probe] = iterations
            if i in surplus:
                nodeCount[probe] += surplus[i]

    # edge count
    edgeCount = {}
    files = glob(resultPath + '/*')
    i = 1
    for f in files:
        # print f
        readBoincResults(f, edgeCount, whitelist=lgnNodes)

        if verbose:
            print 'reading results\t' + str(i) + '/' + str(len(files)) + '\tedges\t' + str(len(edgeCount))

        i += 1

    # generates the output tables, computing relative frequencies, retrieving classes and stuff..

    tableHeader1 = [['rank', 'x', 'y', 'Fabs', 'Frel', 'intra']]  # TABLE 1 (INTERACTIONS)
    table1 = []
    tableHeader2 = [['rank', 'node', 'Fabs', 'Frel', 'Class']]  # TABLE 2 (EXPANSION)
    table2 = []
    classes = parser.readFemClasses(classesPath)
    geneExpansionList = {}
    i = 1

    for (x, y), count in edgeCount.iteritems():
        # does not consider edges unrelated with the lgn
        if x in lgnNodes or y in lgnNodes:
            relFreq = count / float(min(nodeCount[x], nodeCount[y]))
            intra = x in lgnNodes and y in lgnNodes
            table1.append([x, y, count, relFreq, intra])

            if not intra:
                node = y if x in lgnNodes else x
                cl = classes[node] if node in classes else '??'

                if node in geneExpansionList:
                    geneExpansionList[node][1] += count
                    geneExpansionList[node][2] += relFreq / len(lgnNodes)
                else:
                    geneExpansionList[node] = [node, count, relFreq / len(lgnNodes), cl]

            if verbose:
                print 'computing output\t' + str(i) + '/' + str(len(edgeCount))

            i += 1

    table2 = list(geneExpansionList.values())

    # sorting and output
    table1.sort(key=lambda x: x[3], reverse=True)  # su Frel
    for i in range(len(table1)):
        table1[i] = [i + 1] + table1[i]

    table2.sort(key=lambda x: x[2], reverse=True)  # su Frel
    for i in range(len(table2)):
        table2[i] = [i + 1] + table2[i]

    utilities.tableToFile(outPath + '.interactions', tableHeader1 + table1, header=header)
    utilities.tableToFile(outPath + '.expansion', tableHeader2 + table2, header=header)


if __name__ == '__main__':
    from sys import argv

    if len(argv) - 1 != 5:
        print '''Error! Usage:
        python parserBoinc.py PARAMS
        - runPath
        - lgnPath
        - expressionPath
        - classesPath
        - outPath'''
    else:
        runPath, lgnPath, expressionPath, classesPath, outPath = argv[1:]
        boincPost(runPath + '/results/', runPath + '/parameters.txt', runPath + '/frequency.txt', lgnPath, expressionPath, classesPath, outPath, verbose=True)
