Rank_aggregation
================

The folder of all the stuffs regarding ranking aggregation linked to the course of "Laboratory of biological data mining" @ UNITN by Enrico Blanzieri



Documentation
================

all the Python documentation is in the folder pydoc_source/ _build/html, please refer to it 

all the R documentation is in the folder Rdoc/, please refer to it
