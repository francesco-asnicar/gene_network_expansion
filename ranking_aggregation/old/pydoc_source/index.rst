.. Rank_aggregation documentation master file, created by
   sphinx-quickstart on Fri Dec  5 17:40:46 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to rank_aggregation and rank_extractor's documentation!
============================================

The R functions used by these modules are created by Andrea Argentnini and Caterina Gallo.
The python interface is made by Luca Erculiani.

This code isn't public, you don't have the permission to spread this without the
explicit consensus of the authors 

Contents:

.. toctree::
   :maxdepth: 4

   rank_aggregation
   rank_extractor


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



