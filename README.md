# Gene Network Expansion

This repository contains all the software and scripts developed during the "Laboraty of Biological data-mining" course (a.y. 2013/2014) at the University of Trento, during the "Research poject" activity (first semester of 2014), and during my PhD (2014/201?).

This repository, in particular, contains the PC (Peter-Clark) algorithm implemented in C++, friendly called **PC++**. The original version of the algorithm that we used is present into the *"pcalg"* package in R.
We also developed the **PC++** version for running inside the BOINC environment.
Finally, we developed several scripts for the BOINC server.

When I say *"we"*, I'm referring to the original groups of students that develop this project that are: *Francesco Asnicar*, *Luca Masera*, *Paolo Morettin*, *Nadir Sella*, and *Thomas Tolio*.

## Repository organization

* `README.md`
    * This very same file!
* `boinc_todo.md`
    * A to-do list for the code that regards the BOINC part of the project
* `data/`
    * Contains (and will contain) the experiment file, the LGNs, and the classification mappings. Follow the current naming of file to distinguish them between different organisms
* `dll/`
    * Contains two dll, needed for the windows version of the PC++
* `pc/`
    * Contains all the source code and the Makefiles for compiling the PC++ stand-alone implementation for Linux (32 and 64 bit, kernel versione 3.x), Windows (32 and 64 bit, from Windows XP to Windows 8), and Mac OSX (cpu Intel, from Mac OSX 10.5)
* `pc_boinc/`
    * Contains all the source code, Makefiles, and some scripts for compiling the PC++ BOINC version for Linux (32 and 64 bit, kernel versione 3.x), Windows (32 and 64 bit, from Windows XP to Windows 8), and Mac OSX (cpu Intel, from Mac OSX 10.5)
* `pc_todo.md`
    * A to-do list for the stand-alone implementation of the PC++
* `post_processing/`
    * Contains the scripts that post-process the results of the PC-IMs executed in the BOINC environment (gene@home). There is also some utilities for updating the classification files, for finding the unclassified probes, and for producing plots.
* `scripts_boinc/`
    * Contains the software and the database schema produced for the server-side of the gene@home BOINC project.
* `simple_corr/`
    * Contains the source code of the algorithm that calculate and return a list of arcs (couple of nodes) sorting according to their absolute (positive and negative correlation are treated in the same way) Pearson correlation coefficient.

## How to...

### build the PC++ (stand-alone version)

### build the PC++ (BOINC version)

### build simple_corr

### execute the post-processing

```
#!bash

./postprocessing.py -i 54_At,63_At -p /tmp/test/ -l ../data/athaliana/fos/at_fos_lgn.csv -e ../data/athaliana/at_experiments.csv -c ../data/athaliana/fos/at_fos_classes.csv --ncpu 2
```

```
#!bash

./postprocessing.py -i pcim_ids.txt -p /tmp/test/ -l ../data/athaliana/fos/at_fos_lgn.csv -e ../data/athaliana/at_experiments.csv -c ../data/athaliana/fos/at_fos_classes.csv --ncpu 2
```

### generate the plots

### compute the pre-processing

### execute the ranking-aggregation step

Bash:
```
#!bash

cd ranking_aggregation/Source 
python
```

In the following command "AppY" contains all the files ".expansion". Python console:

```
#!python

import rank_extractor
from rank_aggregation import Rank_aggregator

rank_list = rank_extractor.extractor("AppY", 100)
r=Rank_aggregator(data=rank_list, col_metadata=0, data_header=False, k_max=100)
r.mc4_ranker()
```