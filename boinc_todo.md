# BOINC to-do list
The tasks, ordered by priority, that should be developed in the BOINC version of the PC++, or any other scripts for the BOINC server or the post-processing phase.

## HIGH
- Non killa l’app, se vienegt sospesa nei primi momenti dell’esecuzione


## MEDIUM
 - Progress-bar (Win & Mac) matta nei primi momenti (forse problema del boinc client)


## LOW

 - Terminare i commenti sul progetto “with BOINC”
 - Windows, compilare con mingw
 - ‘testAndRemove’ condizione per togliere l’arco va modificata circa così: ‘if (pVal - alpha > epsilon)’
 - Provare a lanciare il ‘benchmark’ di BOINC mentre si esegue una work-unit, vediamo se BOINC incasina tutto!!!


## DONE 
 - Testare priority db
 - Rendere automatica l’assimilazione dei results (da sample_results) nella cartella che contiene le esecuzioni dei PC-IM
 - TestBOINC: verificare file input gzip (non funziona!), nuovo input-template e nuovo stderr [in corso..]
 - Testare nuovo work generator
 - Capire perchè lo score è diverso
 - Modificare database
 - Salvare lo score al checkpoint
 - Togliere il file “experiments.txt”, mettere il contenuto nella prima riga del file “tile.txt”
 - Mettere “<report_on_rpc/>” invece di “<no_delete/>” nei file sticky
 - Cambiare il DB: togliere la stringa come chiave primaria per la tabella “experiments” e sostituirla con un auto_increment
 - Debug application con multiple files Arabidopsis su BOINC
 - Sembra non sospendere nei primi momenti (fino al primo checkpoint)
 - Fix username e password per utente “ugene” per db “gene”
 - Windows, link statico o dll?
 - Valutare se implementare compressione file lato client (forse basta un tag nel result template)
 - Pensare a qualche soluzione per il problema legato ai file di input troppo pesanti da scaricare (rifare pre-processing!)
 - Preparare due VMs con Ubuntu 8.04 (32 e 64 bit) e provare a compilare l’app BOINC. Verificare con “objdump -T pc.exe | grep memcpy” la version di glibc restituita.
 - Verificare se esiste parametro per g++ in modo da compilare 32 bit su sistema a 64 bit
 - Per il problema di versione di glibc, il server BOINC usa il kernel 3.2.0.x ed e’ a 64 bit. Possiamo provare a compilare l’eseguibile linux-64 sul server BOINC (app v0.04 presenta ancora problemi!)
 - Sistemare la stampa stderr del “Reading checkpoint [..]”, manca “\n” perché’ rimosso dagli timestamps
 - funzione che legge il checkpoint, prima di tornare il valore 0 (quindi la wu viene calcolata dall’inizio) aprire in scrittura il file di output. Pialliamo cosi’ eventuali errori che avvengono durante la lettura del file e non appendiamo risultati a roba calcolata in precedenza.. Insomma fidatevi che e’ meglio!
 - E’ rimasto il define del filename dello score.. Va rimosso!
 - Cambiare “writed” con “wrote”
 - Togliere il file di output score, con conseguente libreria per l’md5 e la variabile nella classe Graph per il conteggio di ‘testAndRemove’
 - Provare a ridurre le chiamate a ‘boinc_is_standalone()’, assegnano il risultato ad un bool e utilizzando questo bool attraverso il programma. Probabilmente migliora le performance quando l’eseguibile viene usato fuori (forse anche dentro) BOINC.
 - testare su vecchie versioni di linux e windows.
 - fornire requisiti minimi.
 - input tile indue start from 0
 - Aumentare le stampe fatte su ‘cerr’
 - Cifrare i punteggi assegnati
 - L’opzione ‘Suspend’ di BOINC non sospende la work-unit
 - Stesura del contratto per rilascio progetto a FEM
 - Controllare e sistemare lo script in python che esegue la prima parte del PC-IM
 - Windows version
 - Inserimento copyright prima di fornire il codice alla FEM (aspettiamo mercoledì’ per poterne parlare con Lino Giusti)
 - Controllare funzionamento progress bar e scrittura dei file checkpoint.txt e points.txt, quando si esegue sotto BOINC
 - Mac version
