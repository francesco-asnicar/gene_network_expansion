#command to pass in the terminal "python for_similarities.py <file resulted after mc4> <file output>"
#example "python for_similarities.py 100-100TrpR output.csv"
#returns the file useful for compute similarities: only the names of the first 100 target genes


import sys
import os
import re
import shutil

inFile = sys.argv[1]
outFile = sys.argv[2]

tf = open('tmp', 'a+')
with open(inFile) as f:
    for line in f.readlines():
	build = re.sub ('\'','',line)
        tf.write(build)
tf.close()
f.close()
shutil.copy('tmp', inFile)
os.remove('tmp')

tf = open('tmp', 'a+')
with open(inFile) as f:
    for line in f.readlines():
	build = re.sub ('(\d){3}.(\d)\)\, \(','',line)
	tf.write(build)
tf.close()
f.close()
shutil.copy('tmp', inFile)
os.remove('tmp')

tf = open('tmp', 'a+')
with open(inFile) as f:
    for line in f.readlines():
       	build = re.sub ('(\d){2}.(\d)\)\, \(','',line)	
	tf.write(build)
tf.close()
f.close()
shutil.copy('tmp', inFile)
os.remove('tmp')

tf = open('tmp', 'a+')
with open(inFile) as f:
    for line in f.readlines():
       	build = re.sub ('(\d){1}.(\d)\)\, \(','',line)	
	tf.write(build)
tf.close()
f.close()
shutil.copy('tmp', inFile)
os.remove('tmp')

tf = open('tmp', 'a+')
with open(inFile) as f:
    for line in f.readlines():
       	build = re.sub ('\[\(','',line)
	tf.write(build)
tf.close()
f.close()
shutil.copy('tmp', inFile)
os.remove('tmp')

tf = open('tmp', 'a+')
with open(inFile) as f:
    for line in f.readlines():
       	build = re.sub ('\)\]','',line)
	tf.write(build)
tf.close()
f.close()
shutil.copy('tmp', inFile)
os.remove('tmp')



count = 0
string = ""
with open(inFile, 'a+') as f:
	while count < 100:			
		c = f.read(1)
		string = string + c
	
		print c
		if c == ",":
			count = count +1
			print "num" ,count 
		if count == 100:
			string	= string [:-1]
	
f.close()
text_file = open(outFile, "w")
text_file.write(string)
text_file.close()


tf = open('tmp', 'a+')
with open(outFile) as f:
    for line in f.readlines():
       	build = re.sub (' ','',line)
	tf.write(build)
tf.close()
f.close()
shutil.copy('tmp', outFile)
os.remove('tmp')

