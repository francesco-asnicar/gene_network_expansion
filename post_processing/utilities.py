import glob
'''
This file contains utilities functions.

'''


def scanFolder(path):
    '''
    Given a path, returns the list of all the files on it.

    '''
    if not path.endswith('/'):
        path += '/'

    return [path+f for f in glob.iglob(path+'*')]


def tableToFile(path, rows, separator=',', header=None):
    '''
    Given a path, a list of lists representing a table,
    print it to file. Optionally, the separator between columns can be specified,
    and a string header.

    '''
    with open(path, 'w') as f:
        if header is not None:
            f.write(header + '\n')
        for row in rows:
            line = str(row[0])
            for element in row[1:]:
                line += separator + str(element)
            f.write(line + '\n')


def edgeToString(edgeTuple):
    '''
    Given an edge stored as a tuple, returns the string representing it.

    '''
    return str(edgeTuple[0]) + ',' + str(edgeTuple[1])


def getCombinations(s, k, d):
    '''
    Given a set s, and two integers k and d, this returns a list of d unique k-sized subsets of s.

    '''
    combinations = []
    if d <= factorial(len(s)) / (factorial(k) * factorial(len(s) - k)) and k <= len(s):
        while len(combinations) < d:
            messedIndexes = list(s)
            shuffle(messedIndexes)
            while len(messedIndexes) >= k:
                new, messedIndexes = set(messedIndexes[:k]), messedIndexes[k:]
                if not new in combinations:
                    combinations.append(new)

    return combinations[:d]
