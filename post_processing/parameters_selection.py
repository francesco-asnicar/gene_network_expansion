#!/usr/bin/env python


import os
import sys
from time import time
from glob import iglob
from argparse import ArgumentParser


__date__ = '27 Mar 2015'
__email__ = 'f.asnicar@unitn.it'
__author__ = 'Francesco Asnicar'
__version__ = '0.01'


SUCCESS = 0
INVALID_ARGS = 1
FOLDER_NOT_FOUND = 2


def info(s, init_new_line=False):
    if s:
        nfo = '\n' if init_new_line else ''
        nfo += '[i] '
        sys.stdout.write(nfo + str(s) + '\n')
        sys.stdout.flush()


def error(s, init_new_line=False):
    if s:
        err = '\n' if init_new_line else ''
        err += '[e] '
        sys.stderr.write(err + str(s) + '\n')
        sys.stderr.flush()


def read_params():
    """
    Parse the input parameters, performing some validity check.
    Return the parsed arguments.
    """
    parser = ArgumentParser(description="parameters_selection.py (ver. "+__version__+" of "+__date__+"). Author: "
        +__author__+" ("+__email__+")")

    parser.add_argument('input_folders', nargs='+', type=str, help="", metavar='INPUT_FOLDERS')

    # name or flags
    # action - The basic type of action to be taken when this argument is encountered at the command line.
    # nargs - The number of command-line arguments that should be consumed.
    # const - A constant value required by some action and nargs selections.
    # default - The value produced if the argument is absent from the command line.
    # type - The type to which the command-line argument should be converted.
    # choices - A container of the allowable values for the argument.
    # required - Whether or not the command-line option may be omitted (optionals only).
    # help - A brief description of what the argument does.
    # metavar - A name for the argument in usage messages.
    # dest

    args = parser.parse_args()

    # Additional checks for input params

    return args


def jaccard_similarity(A_list, B_list):
    s = 0.0

    for n in xrange(1, len(A_list)+1):
        s += float(len(set(A_list[:n]).intersection(set(B_list[:n])))) / len(set(A_list[:n]).union(set(B_list[:n])))

    try:
        return s/len(A_list)
    except:
        error('cannot compute similarity: s='+str(s)+', len='+str(len(A_list))+'. Return "nan"')
        return float('nan')


def main(args):
    """
    """
    if not args.input_folders:
        error('invalid argument: "'+str(args.input_folders)+'"')
        return INVALID_ARGS

    for folder in args.input_folders:
        if not os.path.isdir(folder):
            error('folder does not exists: "'+str(folder)+'"')
            return FOLDER_NOT_FOUND

    for folder in args.input_folders:
        if not folder.endswith('/'):
            folder += '/'

        # only one aggregated result is expected
        aggregated = [f.replace(folder, '', 1).strip() for f in iglob(folder+'*.csv') if 'similarities' not in f][0]
        pcims = [f.replace(folder, '', 1).strip() for f in iglob(folder+'*.expansion')]
        expansion_lists = {}
        listt = []

        with open(folder+aggregated, 'rU') as f:
            for r in f:
                listt += [rr.strip() for rr in r.strip().split(',') if rr]

        expansion_lists[aggregated] = listt

        for pcim in pcims:
            listt = []
            with open(folder+pcim, 'rU') as f:
                for r in f.readlines()[2:]:
                    listt.append(r.strip().split(',')[1])

            expansion_lists[pcim] = listt[:len(expansion_lists[aggregated])]

        output_file = folder+aggregated[:aggregated.rfind('.')]+"_similarities.csv"

        if os.path.exists(output_file):
            info('file already exists, will be overwritten: "'+output_file+'"')

        with open(output_file, 'w') as f:
            f.write(','.join(['aggregated'] + sorted(pcims))+'\n')
            similarities = []

            for pcim in sorted(pcims):
                similarities.append("{0:.2f}".format(jaccard_similarity(expansion_lists[aggregated], expansion_lists[pcim])))

            f.write(aggregated+','+','.join(similarities)+'\n')

        info('similarities file written: "'+output_file+'"')

    return SUCCESS


if __name__ == "__main__":
    t0 = time()
    args = read_params()
    status = main(args)
    info("total time: "+str(int(time()-t0))+"s")
    sys.exit(status)
