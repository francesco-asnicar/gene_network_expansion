from glob import glob

files = glob('*.expansion')
pcim = dict()

for f in files:
    lines = []

    with open(f, 'r') as fin:
        lines = fin.readlines()

    description = lines[0].split(' ')[1].strip()
    alpha = lines[0].split(' ')[2].strip()
    iterations = lines[0].split(' ')[3].strip()
    tile_size = lines[0].split(' ')[4].strip()
    expansion_list = [s.split(',')[1].strip() for s in lines[2:57]]
    pcim[(description, float(alpha), int(tile_size), int(iterations))] = expansion_list


with open('bigtable.csv', 'w') as fout:
    fout.write("description,alpha,tile size,iterations,expansion list\n")

    for d, a, t, i in sorted(pcim, key=lambda x: (x[1], x[3], x[2])):
        fout.write(','.join([d, str(a), str(t), str(i)] + pcim[(d, a, t, i)]) + '\n')
