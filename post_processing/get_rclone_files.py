#! /usr/bin/python

import argparse
import os
import subprocess as sp
import sys
import time
from Queue import Queue, Empty
from threading import Thread


def stream_watcher(identifier, stream):
    while True:
        line = stream.readline()
        io_q.put((identifier, line))
        if not line:
            break

    if not stream.closed:
        stream.close()


def printer():
    total = len(experiments_ids) * (1 if args.raw_data else 2)
    checked = 0
    copied = []
    while True:
        try:
            item = io_q.get(True, .1)
        except Empty:
            if proc.poll() is not None:
                break
        else:
            identifier, line = item
            if 'Copied' in line:
                copied.append(line.split(': ')[1].strip())
                sys.stdout.write('[{:>{s_len}}/{:>{s_len}}]\t{}\n'.format(len(copied) + checked, total, line.split(': ')[1], s_len=len(str(total))))
            elif 'Checks' in line:
                checked = int(line.split(':')[1].strip())
                sys.stdout.write('[{:>{s_len}}/{:>{s_len}}]\t{} found locally\n'.format(len(copied) + checked, total, checked, s_len=len(str(total))))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('experiments_id_list', type=str, help='File containing the list of experiment\'s ids to download')
    parser.add_argument('output_folder', type=str, help='Output folder')
    parser.add_argument('-m', '--rename_map', type=str, help='File containing a renaming map in the format remote_name [tab] local_name')
    parser.add_argument('--keep_both', action='store_true', help='If output file map is specified, use this flag to keep both versions')
    parser.add_argument('--raw_data', action='store_true', help='If output file map is specified, use this flag to keep both versions')

    parser.add_argument('--rclone_drive', type=str, default='unitn_drive', help='')

    args = parser.parse_args(sys.argv[1:])

    sys.stderr.write('Checking inputs...')

    try:
        sp.check_call(['rclone', 'lsd', '{}:'.format(args.rclone_drive), '--max-depth', '0'], stdout=open(os.devnull, "w"))
    except Exception as exc:
        sys.stderr.write('Rclone is not installed or correctly configured on this machine.\n')
        sys.stderr.write(
            'Please ask f.asnicar@unitn.it access to the GDrive folder and follow the instructions on the page http://rclone.org/install/ for the installation and configuration of Rclone.')
        sys.exit(1)

    # TODO check input file format: no extension
    experiments_ids = [l.strip() for l in open(args.experiments_id_list)]

    if args.rename_map:
        try:
            file_map = dict(tuple(l.strip().split('\t')) for l in open(args.rename_map))
            assert not (set(experiments_ids) - set(file_map.keys()))
        except Exception:
            sys.stderr.write('\n You file name mapping seems not well formatted.')
            sys.exit(2)

    sys.stderr.write('\b\b\b: OK\n')

    sys.stderr.write('Downloading required files...\n')

    io_q = Queue()

    if args.raw_data:
        to_download = os.path.join(args.output_folder, 'to_download')
        remote_files = ['{0}.tar'.format(exp_id) for exp_id in experiments_ids]
        open(to_download, 'w').write('\n'.join(remote_files))

        sp.check_call(['rclone', 'copy', '{}:experiments_raw_data'.format(args.rclone_drive), args.output_folder, '--files-from', to_download], stdout=sys.stdout)
    else:
        to_download = os.path.join(args.output_folder, 'to_download')
        remote_files = ['{0}.interactions'.format(exp_id) for exp_id in experiments_ids]
        remote_files += ['{0}.expansion'.format(exp_id) for exp_id in experiments_ids]
        open(to_download, 'w').write('\n'.join(remote_files))

        proc = sp.Popen(['rclone', '-v', 'copy', '{}:experiments_results'.format(args.rclone_drive), args.output_folder, '--files-from', to_download], stdout=sp.PIPE, stderr=sp.PIPE)

        # Thread(target=stream_watcher, name='stdout-watcher', args=('STDOUT', proc.stdout)).start()
        Thread(target=stream_watcher, name='stderr-watcher', args=('STDERR', proc.stderr)).start()

        Thread(target=printer, name='printer').start()

        proc.wait()
        time.sleep(.5)

    sys.stderr.write('Downloading required files: OK\n')

    os.remove(to_download)

    if args.rename_map:
        sys.stderr.write('Renaming files...')
        for exp_id in experiments_ids:
            if args.raw_data:
                os.rename(os.path.join(args.output_folder, '{}.tar'.format(exp_id)), os.path.join(args.output_folder, '{}.tar'.format(file_map[exp_id])))
            else:
                os.rename(os.path.join(args.output_folder, '{}.expansion'.format(exp_id)), os.path.join(args.output_folder, '{}.expansion'.format(file_map[exp_id])))
                os.rename(os.path.join(args.output_folder, '{}.interactions'.format(exp_id)), os.path.join(args.output_folder, '{}.interactions'.format(file_map[exp_id])))
        sys.stderr.write('\b\b\b: OK\n')
