from sys import argv, exit
from os.path import isfile


def precision(lst, classes):
    c12 = 0
    c34 = 0

    for e in lst:
        if classes[e] in ['Class 1', 'Class 2']:
            c12 += 1
        else:
            c34 += 1

    return float(c12)/(float(c12) + float(c34))


if __name__ == '__main__':
    if len(argv) != 3:
        print "ERROR: invalid number of argumets"
        print "Usage:"
        print "  $ python precision.py <class_file> <bigtable_file>"
        print ""
        exit(1)

    if not isfile(argv[1]):
        print "ERROR:", argv[1], "is not a valid file or does not exists"
        exit(1)

    if not isfile(argv[2]):
        print "ERROR:", argv[2], "is not a valid file or does not exists"
        exit(1)

    k = [1, 5, 10, 20, 44, 55]
    classes = dict()
    pcim = dict()
    precisions = dict()

    # load classes file
    with open(argv[1], 'r') as fin:
        classes = dict(map(lambda s: tuple(s.strip().split(',')), fin.read().strip().split('\n')))

    # load the pcims from bigtable
    '''
    description,alpha,tile size,iterations,expansion list,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
    '''
    with open(argv[2], 'r') as fin:
        for r in fin.readlines()[1:]:
            description = r.split(',')[0].strip()
            alpha = r.split(',')[1].strip()
            tile_size = r.split(',')[2].strip()
            iterations = r.split(',')[3].strip()
            pcim[(description, alpha, tile_size, iterations)] = [s.strip() for s in r.split(',')[4:] if s.strip()]

    # compute the precisions
    for p in pcim:
        precisions[p] = []

        for kk in k:
            if kk <= len(pcim[p]):
                precisions[p].append(str(precision(pcim[p][:kk], classes)))

    with open('precisions.csv', 'w') as fout:
        fout.write("description,alpha,tile size,iterations,k1,k5,k10,k20,k44,k55\n")

        for d, a, t, i in sorted(precisions, key=lambda x: (x[1], x[3], x[2])):
            fout.write(','.join([d, str(a), str(t), str(i)] + precisions[(d, a, t, i)]) + '\n')
