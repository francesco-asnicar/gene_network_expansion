#!/bin/bash


# cd /data/tmp/
# rclone ls "gDrive unitn":experiments_raw_data/ | rev | cut -f1 -d' ' | rev > experiments_gdrive.txt

for i in $(cat experiments_gdrive.txt); do
    echo -n "${i} "
    rclone --quiet copy "gDrive unitn":experiments_raw_data/${i} .
    tar -xf ${i}

    if [ -d ${i::-4} ]; then
        echo "Ok"
        rm -r ${i} ${i::-4}
    else
        if [ -d downloads ]; then
            echo "downloads"
            cd downloads/
            tar -cf ${i} ${i::-4}
            rclone copy ${i} "gDrive unitn":experiments_raw_data/
            cd ../
            rm -r ${i} downloads/
        else
            echo
        fi
    fi
done
