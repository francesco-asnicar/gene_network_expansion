
'''
This file contains generic parsing functions.
'''


def readLGNFile(lgnPath) :
	'''
	Given the LGN file path, returns the list of edges of the lgn as tuples of strings.
	If the file's not open, returns None.

	'''
	lgn = []
	# print "lgnPath", lgnPath

	with open(lgnPath, 'r') as f:
		for line in [r for r in f][1:]:
			edge = tuple(map(lambda s: s.lower().strip(), line.split(',')))
			# edge = tuple(map(lambda s: s.strip(), line.split(',')))
			lgn.append(edge)

	return lgn


def readFemClasses(path) :
	'''
	Given the FEM Classes file path, returns a dictionary containing,
	for each probe ID, the corresponding class.
	If the file's not open, returns None.

	'''
	classes = {}
	with open(path) as classesFile :
		for line in classesFile :
			if line != "" :
				probe, cl = line.lower().strip().split(',')
				# probe, cl = line.strip().split(',')
				classes[probe] = cl
		return classes


def readProbeIds(expressionPath) :
	'''
	Given the path of the expression data file,
	returns a dictionary containing, for each row index, the corresponding probe ID.
	If the file's not open, returns None.

	'''
	with open(expressionPath,'r') as f :
		lines = [r for r in f]
		return dict(zip(range(len(lines)), [line.split(',')[0].lower() for line in lines[1:] if line]))
		# return dict(zip(range(len(lines)), [line.split(',')[0] for line in lines[1:] if line]))

def readResultFile(path,nodeCount,edgeCount,whitelist=None) :
	'''
	This reads an output file, updating two dictionaries. The first contains an entry for
	each node and it's frequency, the second contains the frequency of the edges.
	If the file's not open, it leaves the dictionaries untouched.

	PARAMS:
	nodeCount - the dictionary containing the node frequencies in the blocks.
	edgeCount - the dictionary containing the edge frequencies in the output files.
	[whitelist] - optional list of nodes to filter the edgeCount, if an edge contains at least
		a node from whitelist (e.g. LGN), then it's counted. by default, no filtering is done on the edges.

	'''

	with open(path,'r') as f :

		for line in f :

			# line's a header
			if line[0] == '#' :
				for node in map(lambda s : s.lower.strip(),line[2:].split(', ')) :
					if node in nodeCount :
						nodeCount[node] += 1
					else :
						nodeCount[node] = 1
			else :
				edge = tuple(map(lambda s : s.lower.strip(),line.split(',')))
				if not whitelist or (edge[0] in whitelist or edge[1] in whitelist) :
					if edge in edgeCount :
						edgeCount[edge] += 1
					else :
						edgeCount[edge] = 1
