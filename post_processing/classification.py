from sys import argv
from sys import exit
from os.path import isfile


def load_classes(filename):
    # load classification
    classes = dict()

    with open(filename, 'r') as f:
        classes = dict(map(lambda s: tuple(s.strip().split(',')), f.read().strip().split('\n')))

    return classes


def load_probes(filename, header=True, separator=',', position=0):
    # load probes
    probes = []
    fromm = 1 if header else 0

    with open(filename, 'r') as f:
        probes = [l.strip().split(separator)[position] for l in f.readlines()[fromm:]]

    return probes


if __name__ == '__main__':
    if len(argv) != 3:
        print "ERROR: invalid number of argumets"
        print "Usage:"
        print "    $ python classification.py <class_file> <probe_list_file>"
        print "It will print the list of probes for which the <class_file> does not have a class assigned"
        print 'If all probes have a class it will print \"All probes are classified\"'
        exit(1)

    if not isfile(argv[1]):
        print "ERROR:", argv[1], "is not a valid file or does not exists"
        exit(1)

    if not isfile(argv[2]):
        print "ERROR:", argv[2], "is not a valid file or does not exists"
        exit(1)

    classes = load_classes(argv[1])
    probes = load_probes(argv[2])
    not_classified = [p for p in probes if p not in classes]

    if not_classified:
        print "Not classified probes:"
        print ', '.join(not_classified)
    else:
        print "All probes are classified"

    exit(0)
