#!/bin/bash


# check the number of lines in the result files
# if the number of lines is equal to 2, it means that no results have been
# computed for that experiment

> check_empty_results.txt

for i in $(ls results/); do
    ll=`cat results/${i} | wc -l`

    if [[ $ll -eq 2 ]]; then
        echo ${i} >> check_empty_results.txt
    fi
done
