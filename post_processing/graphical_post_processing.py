#!/usr/bin/env python


import matplotlib
matplotlib.use('PS')
import matplotlib.pyplot as plt
# from matplotlib.backends.backend_pdf import PdfPages
import sys
# import datetime
import getopt
import pickle
import random
import pylab
import numpy
from glob import iglob
from os import listdir, path, makedirs, getcwd
from os.path import isfile, join
# from math import ceil, fabs
from matplotlib.font_manager import FontProperties
# from scipy.weave.base_info import info_list


# Type 3 fonts

matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

# os.environ['PATH'] = os.environ['PATH'] + ':/usr/texbin'

# matplotlib.rcParams['ps.fonttype'] = 42

# plt.rc('ps', fonttype=1)



def jaccard_similarity(A_list, B_list) :
    s = 0.0
    for n in xrange(1, len(A_list) + 1) :
        s += float(len(set(A_list[ :n]).intersection(set(B_list[ :n])))) / len(set(A_list[ :n]).union(set(B_list[ :n])))
    try :
        return s / len(A_list)
    except :
        return float('nan')


def plot_precision(infos, relation_classes, top, num_iteration, output_file,colors_flag=False) :
    """
    :type infos : list
    :type relation_classes : dict
    :type top : int
    :type num_iteration : int
    :type output_file : str
    """
    # this function produces a precision plot, with a curve for each combination of parameters (but fixed number of iterations)
    # up to a given 'top' threshold
    # PARAMS:
    # info - a list of tuples (file_name,lgn,alpha,iterations,tile_size)
    # relation_classes - a dictionary containing the class annotations, indexed by gene ID
    # top - upper bound to the rankings.
    # num_iteration - integer that fixes the desired number of iteration
    # output_file - the output filename, without extension

    # a dictionary (tile_size,lgn,alpha) -> [precision], containing the list of precisions (with threshold k) for each tuple of params (tile_size,lgn,alpha)
    rankings = {(x[4], x[1], x[2]) : [0.0 for _ in xrange(top)] for x in infos if x[3] == num_iteration}
    # dictionary that contains, for each tuple (filename,lgn,alpha,iter,tilesize), the set of unclassified genes
    unclass_by_params = {x : set() for x in infos}
    # dictionary that contains, for each different iterations value, the set of unclassified genes
    unclass_by_it = {it : set() for it in {x[3] for x in infos}}
    # the set of all the unclassified genes
    unclass_genes = set()

    for info_tuple in infos :
        file_name, lgn, alpha, it, ts = info_tuple

        # reads every output file (regardless of the iteration number)
        with open(file_name, 'r') as f :
            lines = f.readlines()[2 :top + 2]
            i = 0
            for line in lines :
                rank, gene, _, freq, _ = tuple(map(lambda s : s.strip(), line.split(',')))
                # searches for the class annotation. if absent, the gene is added in the unclass* data structures
                rel = relation_classes[gene] if gene in relation_classes else '??'
                if rel == '??' :
                    unclass_by_params[info_tuple].add(gene)
                    unclass_by_it[it].add(gene)
                    unclass_genes.add(gene)
                # partially computes the precision only for the right iteration number
                # rankings lists contain the number of TP at each threshold, calculated recursively
                if it == num_iteration :
                    if i == 0 :
                        rankings[(ts, lgn, alpha)][i] += 1 if rel in ['Class 1', 'Class 2'] else 0
                    else :
                        rankings[(ts, lgn, alpha)][i] = rankings[(ts, lgn, alpha)][i - 1] + (1 if rel in ['Class 1', 'Class 2'] else 0)
                    i += 1

    # unclassified genes raise an exception (regardless of the iteration param)
    if len(unclass_genes) != 0 :
        print "!!! Missing class data !!! (check non_classificati.txt)"
        # raise Exception("Missing class data")

        # writes the unclassified genes in two distinct file
        with open('non_classificati.txt', 'w') as f :
            for k, v in unclass_by_params.items() :
                f.write(str(k) + '\n')
                for x in v :
                    f.write(x + '\t')
                f.write('\n\n')
            f.write('TOTAL\n')
            for x in unclass_genes :
                f.write(x + '\t')
            f.write('\n')

        with open('non_classificati_iter.txt', 'w') as f :
            already_printed = set()
            for k, v in sorted(unclass_by_it.items(),reverse=True) :
                f.write(str(k) + ' iterations\n')
                difference = v - already_printed
                for x in difference :
                    f.write(x + '\t')
                    already_printed.add(x)
                f.write('\n\n')
            f.write('TOTAL\n')
            for x in unclass_genes :
                f.write(x + '\t')
            f.write('\n')

    # computes the proper precision lists, dividing the (previously calculated) TP values by the length (that is TP+FP)
    normalized = sorted(map(lambda x : (map(lambda y : x[1][y] / (y + 1), xrange(len(x[1]))), x[0]), rankings.items()), key=lambda x : (x[1][1], x[1][2], x[1][0]))

    # sets colors or greyscale values
    if colors_flag :
            colors = ['b', 'g', 'r', 'c', 'm', 'y']
    else :
            colors = ['0.1','0.5','0.9']

    markers = ['v', '^', 's', 'h', 'd']

    # i don't get it
    random.seed(42)
    random.shuffle(colors)
    random.shuffle(markers)

    fig = plt.figure(figsize=(10, 10))
    # fig.suptitle('Precision of the top ' + str(top) + ' results with ' + str(num_iteration) + ' iterations', fontsize=20) # title became unwanted
    ax = fig.add_subplot(111)

    # fixed precision of the competitors (hopefully correct)
    PC_gw = 0.3966
    PC_gw_dev = 0.0271
    PCs_gw = 0.4318
    ARACNE = 0.4545

    ax.set_ylim([0, 1.01])
    ax.set_xlim([0.5, top + 0.5])

    # plots each curve with a different with different color and markers
    for l in xrange(len(normalized)) :
        ax.plot(xrange(1, len(normalized[l][0]) + 1), normalized[l][0], color=colors[l % len(colors)], markerfacecolor=colors[l % len(colors)], linestyle=':', marker=markers[l % len(markers)],
                label='PC-IM tile='+str(normalized[l][1][0]),markersize='5.0') # it was 0.8

    # plots the PC star (genome-wide) precision value defined (hardcoded) above
    ax.plot(xrange(1, top + 1), [PCs_gw] * top, color='k', linestyle='-', label='PC*')

    # plots ARACNE precision value defined (hardcoded) above
    ax.plot(xrange(1, top + 1), [ARACNE] * top, color='k', linestyle='-.', label='ARACNE')

    # plots the PC (genome-wide) precision value defined (hardcoded) above, plus the upper/lower bounds
    ax.plot(xrange(1, top + 1), [PC_gw + PC_gw_dev] * top, color='k', linestyle=':', alpha=0.6)
    ax.plot(xrange(1, top + 1), [PC_gw] * top, color='k', linestyle='--')
    ax.plot(xrange(1, top + 1), [PC_gw - PC_gw_dev] * top, color='k', linestyle=':', alpha=0.6)
    ax.errorbar(15, PC_gw, yerr=PC_gw_dev, fmt='o', linestyle='--', color='k', label='PC')

    # sets axis labels, legend position, size, alpha
    ax.set_xlabel('Ranking', fontsize=24)
    ax.set_ylabel('Precision', fontsize=24)
    ax.tick_params(axis='x', labelsize=16)
    ax.tick_params(axis='y', labelsize=16)

    leg = plt.legend(loc='lower left',prop={'size':12})
    leg.get_frame().set_alpha(0.9)

    # saves the precision plot
    fig.savefig(output_file+'.eps',format='eps',dpi=150)

def plot_scatter(infos, relation_classes, top, mode, output_file, colors_flag=False) :
    """
    :type infos : list
    :type relation_classes : dict
    :type top : int
    :type mode : str
    :type output_file : str
    """

    # this function produces the scatter plots
    # the parameters are a subset of the 'plot_precision' ones

    # alpha mode : compares the precisions between alpha 0.01 and 0.05
    # empty mode : compares the precisions between the standard approach and 'empty lgn' approach

    if mode == 'alpha':
        scatterplot_title = 'Alpha 0.01/0.05 scatter plot'
        # alpha_chr = unichr(0x3b1).encode('utf-8')
        # alpha_chr = u"\u03B1"
        alpha_chr = "alpha"
        axis_label = ('PC-IM precision, '+alpha_chr+'=0.01', 'PC-IM precision, '+alpha_chr+'=0.05')

        # rankings dictionary as in plot_precision
        rankings = {(t, i, a): [0.0 for _ in xrange(top)] for _, _, a, i, t in infos}

        for info_tuple in infos :
            file_name, lgn, alpha, it, ts = info_tuple

            with open(file_name, 'r') as f:
                lines = f.readlines()[2:top+2]
                i = 0
                # again, as before, computes the TP up to i-th rank
                for line in lines :
                    rank, gene, _, freq, _ = tuple(map(lambda s : s.strip(), line.split(',')))
                    rel = relation_classes[gene] if gene in relation_classes else '??'
                    if rel == '??' :
                        print "oops! missing class data!"
                        # raise Exception("SCATTERPLOT: missing class data for gene " + gene)

                    if i == 0 :
                        rankings[(ts, it, alpha)][i] += 1 if rel in ['Class 1', 'Class 2'] else 0
                    else :
                        rankings[(ts, it, alpha)][i] = rankings[(ts, it, alpha)][i - 1] + \
                                                                             (1 if rel in ['Class 1', 'Class 2'] else 0)
                    i += 1

        precisions = [[], []]
        # the 2 lists contains tuples (p, (tile_size, iter, i)) where p is the precision of the run with params
        # (tile_size, iter) up to the i-th rank
        # precisions[0] have alpha 0.01, whereas precisions[1] have alpha 0.05

        for (ts, it, alpha) in rankings:
            if alpha == 0.01:
                for i in xrange(len(rankings[(ts, it, alpha)])):
                    precisions[0].append( (rankings[(ts, it, alpha)][i]/(i+1), (ts, it, i)) )
            elif alpha == 0.05:
                for i in xrange(len(rankings[(ts, it, alpha)])):
                    precisions[1].append( (rankings[(ts, it, alpha)][i]/(i+1), (ts, it, i)) )
            else:
                print "wtf!?"
                raise Exception("SCATTERPLOT: unexpected alpha value")

    elif mode == 'empty' :

        scatterplot_title = 'LGN/Empty tile scatter plot'
        axis_label = 'PC-IM precision','Control precision'

        rankings = {(x[4], x[3], x[1]) : [0.0 for _ in xrange(top)] for x in infos}

        for info_tuple in infos :
            file_name, lgn, alpha, it, ts = info_tuple
            with open(file_name, 'r') as f :
                lines = f.readlines()[2 :top + 2]
                i = 0
                for line in lines :
                    rank, gene, _, freq, _ = tuple(map(lambda s : s.strip(), line.split(',')))
                    rel = relation_classes[gene] if gene in relation_classes else '??'
                    if rel == '??' :
                        print "oops! missing class data!"
                        # raise Exception("SCATTERPLOT: missing class data for gene " + gene)

                    if i == 0 :
                        rankings[(ts, it, lgn)][i] += 1 if rel in ['Class 1', 'Class 2'] else 0
                    else :
                        rankings[(ts, it, lgn)][i] = rankings[(ts, it, lgn)][i - 1] + (1 if rel in ['Class 1', 'Class 2'] else 0)
                    i += 1


        precisions = [[],[]]
        # same as before, but now it discriminates between 'fos' and 'empty' lgn parameter
        for (ts,it,lgn) in rankings :
            if lgn.lower() == 'fos' :
                for i in xrange(len(rankings[(ts,it,lgn)])) :
                    precisions[0].append( (rankings[(ts,it,lgn)][i]/(i+1),(ts,it,i)) )
            elif lgn.lower() == 'empty' :
                for i in xrange(len(rankings[(ts,it,lgn)])) :
                    precisions[1].append( (rankings[(ts,it,lgn)][i]/(i+1),(ts,it,i)) )
            else :
                print "wtf!?"
                raise Exception("SCATTERPLOT: unexpected lgn value")

    else : raise Exception("SCATTERPLOT: unknown mode")

    # by sorting the 2 lists, the same parameters are coupled together (if there isn't missing data)
    precisions[0].sort(key=lambda x: x[1]) # alpha 0.01
    precisions[1].sort(key=lambda x: x[1]) # alpha 0.05

    # consistency check -> if the parameters aren't coupled together, raises an exception!
    try:
        for i in xrange(max(len(precisions[0]), len(precisions[1]))):
            if precisions[0][i][1] != precisions[1][i][1]:
                raise IndexError()
    except IndexError:
        raise Exception("SCATTERPLOT: inconsistent input data")

    # SUMMARY SCATTER PLOT
    fig = plt.figure(figsize=(10, 10))
    # fig.suptitle(scatterplot_title, fontsize=20)

    # sets areas and symmetry axis colors
    if colors_flag:
        color = 'b' # b for blue
        sym_axis_color = 'r' # r for red
    else:
        color = '0.5' # in matplotlib, this is the shorter version of (0.5, 0.5, 0.5)
        sym_axis_color = 'k' # k for black

    ax = fig.add_subplot(111)
    ax.set_ylim([0, 1.01])
    ax.set_xlim([0, 1.01])
    ax.plot([0,1], [0,1], color=sym_axis_color)

    precisions[0] = map(lambda x: x[0], precisions[0]) # keep only the precision, alpha 0.01
    precisions[1] = map(lambda x: x[0], precisions[1]) # keep only the precision, alpha 0.05
    plotdata = zip(precisions[0], precisions[1])
    aggregated = list(set(plotdata))
    areas = [50*plotdata.count(t) for t in aggregated]

    ax.scatter(map(lambda x: x[0], aggregated), map(lambda x: x[1], aggregated), s=areas, c=color, alpha=0.3,
                                                                                                         edgecolors='k')
    ax.set_xlabel(axis_label[0], fontsize=22)
    ax.set_ylabel(axis_label[1], fontsize=22)
    ax.tick_params(axis='x', labelsize=15)
    ax.tick_params(axis='y', labelsize=15)
    print "Saving", output_file + '_summary_scatterplot.eps'
    fig.savefig(output_file + '_summary_scatterplot.eps', format='eps', dpi=150)

    # TOP x SCATTER PLOT
    agg_10_20_44_55 = {}

    for x in [1, 5, 10, 20, 44, 55]:
        fig = plt.figure(figsize=(10, 10))
        # fig.suptitle(scatterplot_title + ' (top ' + str(x) + ')', fontsize=20)

        ax = fig.add_subplot(111)
        ax.set_ylim([0, 1.01])
        ax.set_xlim([0, 1.01])
        ax.plot([0, 1], [0, 1], color=sym_axis_color)

        plotdata = [None, None]
        plotdata[0] = [precisions[0][i] for i in xrange(x-1, len(precisions[0])) if (i-x+1)%top == 0]
        plotdata[1] = [precisions[1][i] for i in xrange(x-1, len(precisions[1])) if (i-x+1)%top == 0]
        # plotdata[0] = [precisions[0][i] for i in xrange(x)]
        # plotdata[1] = [precisions[1][i] for i in xrange(x)]
        plotdata = zip(plotdata[0], plotdata[1])
        aggregated = list(set(plotdata))
        areas = [50*plotdata.count(t) for t in aggregated]

        if x in [10, 20, 44, 55]:
            agg_10_20_44_55[x] = (plotdata, aggregated)

        ax.scatter(map(lambda x: x[0], aggregated), map(lambda x: x[1], aggregated), s=areas, c=color, alpha=0.3,
                                                                                                         edgecolors='k')
        ax.set_xlabel(axis_label[0], fontsize=22)
        ax.set_ylabel(axis_label[1], fontsize=22)
        ax.tick_params(axis='x', labelsize=15)
        ax.tick_params(axis='y', labelsize=15)
        print "Saving", output_file + '_top_'+str(x)+'_scatterplot.eps'
        fig.savefig(output_file + '_top_'+str(x)+'_scatterplot.eps', format='eps', dpi=150)

    if agg_10_20_44_55:
        fig = plt.figure(figsize=(10, 10))
        # fig.suptitle(scatterplot_title + ' (top ' + str(x) + ')', fontsize=20)

        ax = fig.add_subplot(111)
        ax.set_ylim([0, 1.01])
        ax.set_xlim([0, 1.01])

        plotdata = []
        aggregated = []
        for _, v in agg_10_20_44_55.iteritems():
            pd, a = v
            plotdata += pd
            aggregated = list(set(aggregated).union(set(a)))

        areas = [50*plotdata.count(t) for t in aggregated]

        ax.plot([0,1], [0,1], color=sym_axis_color)
        ax.scatter(map(lambda x: x[0], aggregated), map(lambda x: x[1], aggregated), s=areas, c=color, alpha=0.3,
                                                                                                         edgecolors='k')
        ax.set_xlabel(axis_label[0], fontsize=22)
        ax.set_ylabel(axis_label[1], fontsize=22)
        ax.tick_params(axis='x', labelsize=15)
        ax.tick_params(axis='y', labelsize=15)
        print "Saving", output_file + '_top_10-20-44-55_scatterplot.eps'
        fig.savefig(output_file + '_top_10-20-44-55_scatterplot.eps', format='eps', dpi=150)


def plot_similarity(result_matrix, label_list, precision, top, output_file) :
    """
    :param result_matrix :
    :param label_list :
    :param precision :
    :param top :
    :param output_file :
    """
    # Mask lower triangle
    # mask = numpy.tril(result_matrix, k=-1)
    # result_matrix = numpy.ma.array(result_matrix, mask=mask)  # mask out the lower triangle

    # fig = pylab.figure(figsize=(20, 20))
    fig = pylab.figure(figsize=(10, 10))
    # fig.suptitle('Similarity matrix of the top ' + str(top) + ' results', fontsize=20)

    # Plot distance matrix.
    # axmatrix = fig.add_axes([0.1, 0.1, 0.75, 0.75])
    axmatrix = fig.add_axes([0.225, 0.15, 0.75, 0.75])

    cmap = pylab.cm.get_cmap('jet')
    cmap.set_bad('w')
    im = axmatrix.matshow(result_matrix, aspect='auto', origin='upper', interpolation="nearest", cmap=cmap, vmin=0, vmax=1)

    axmatrix.xaxis.set_label_position("bottom")
    axmatrix.yaxis.set_label_position("right")

    axmatrix.set_xlabel('tile size, # iteration')  #, fontsize=35)
    axmatrix.set_ylabel('tile size, # iteration')  #, fontsize=35)
    # axmatrix.set_xlabel('PC iterations', fontsize=35)
    # axmatrix.set_ylabel('PC iterations', fontsize=35)

    axmatrix.set_xticks(range(len(label_list[0])))
    axmatrix.set_yticks(range(len(label_list[1])))

    font = FontProperties()
    font1 = font.copy()
    font1.set_family('monospace')
    font1.set_size('x-small')

    font2 = font.copy()
    font2.set_size('x-small')

    axmatrix.set_xticklabels(label_list[0], rotation=90, fontproperties=font1)  #, fontsize=10) #, fontproperties=font)
    axmatrix.set_yticklabels(label_list[1], fontproperties=font1)  #, fontsize=10) #, fontproperties=font)

    # Plot colorbar.
    # axcolor = fig.add_axes([0.91, 0.1, 0.02, 0.75])
    axcolor = fig.add_axes([0.225, 0.070, 0.75, 0.03],label='Similarity')

    cbar = pylab.colorbar(im, cax=axcolor, orientation='horizontal')
    axcolor.set_xlabel('Similarity',labelpad=5)
    # cbar.ax.tick_params(labelsize=35)

    axprec = fig.add_axes([0.01, 0.15, 0.11, 0.75])
    axprec.set_xlim([1, 0])
    axprec.set_ylim([0.5, len(precision[0]) + 0.5])
    # axprec.plot(precision[top - 1], numpy.arange(0.5, len(cases) + 0.5, 1)[ : :-1], color='k', markerfacecolor='k', marker='o', linestyle='-')
    axprec.barh(numpy.arange(1, len(precision[0]) + 1, 1)[ : :-1], precision[top - 1], align='center', alpha=0.5, edgecolor='k', color='k')
    # axprec.yaxis.set_visible(False)
    axprec.set_yticks(range(1, len(precision[0]) + 1))
    axprec.set_yticklabels('' * 50)
    axprec.set_axisbelow(True)
    axprec.xaxis.grid(alpha=0.3)
    axprec.set_xticks(numpy.arange(0, 1.1, 0.25))
    axprec.set_xticklabels(numpy.arange(0, 1.1, 0.25), fontproperties=font2)
    axprec.set_xlabel('Precision')

    fig.savefig(output_file+'.eps',format='eps',dpi=150)
    pylab.close()
    # return fig


def parse_data(top, infos, relation_classes, working_directory, prefix, recompute_policy) :
    """
    :type top : int
    :type infos : list
    :type relation_classes : dict
    :type working_directory : str
    :type prefix : str
    :type recompute_policy : list
    :rtype  : list,list,list
    """
    temp_dir = working_directory + 'TEMP/'
    if not path.exists(temp_dir) :
        makedirs(temp_dir)

    temp_file = temp_dir + prefix + '_' + str(top) + '.tmp'

    recompute = True

    if path.isfile(temp_file) :
        if recompute_policy[0] == '' :
            if raw_input("The temporary files already exists, do you want to override and recompute it? (Y/N)\n").lower() in ['y', 'yes'] :
                recompute = True
                if raw_input("Do you want to apply this policy to the next conflicts? (Y/N)\n").lower() in ['y', 'yes'] :
                    recompute_policy[0] = 'ar'
            else :
                recompute = False
                if raw_input("Do you want to apply this policy to the next conflicts? (Y/N)\n").lower() in ['y', 'yes'] :
                    recompute_policy[0] = 'nr'
        elif recompute_policy[0] == 'nr' :
            recompute = False

    if recompute :
        LGNs = sorted(set([x[1] for x in infos]))
        alphas = sorted(set([x[2] for x in infos]))
        iterations = sorted(set([x[3] for x in infos]))
        tile_sizes = sorted(set([x[4] for x in infos]))

        cases = sorted([(x[3], x[4], x[2], x[1]) for x in infos], key=lambda x : (x[3], x[1], x[0], x[2]))

        rankings = {it : {ts : {a : {l : [] for l in LGNs} for a in alphas} for ts in tile_sizes} for it in iterations}
        precision = [[0 for t in cases] for _ in xrange(top)]

        for info_tuple in infos :
            file_name, lgn, alpha, it, ts = info_tuple
            with open(file_name, 'r') as f :
                lines = f.readlines()[2 :top + 2]
                i = 0
                for line in lines :
                    _, gene, _, freq, _ = tuple(map(lambda s : s.strip(), line.split(',')))
                    rel = (relation_classes[gene] if gene in relation_classes else '??').lower()
                    rankings[it][ts][alpha][lgn].append(gene)
                    if i == 0 :
                        precision[i][cases.index((it, ts, alpha, lgn))] = (1 if rel in ['class 1', 'class 2'] else 0) / (i + 1.0)
                    else :
                        precision[i][cases.index((it, ts, alpha, lgn))] = ((precision[i - 1][cases.index((it, ts, alpha, lgn))] * i) + (1 if rel in ['class 1', 'class 2'] else 0)) / (i + 1.0)
                    i += 1

        # computes result similarities
        similarity = {}
        label_list = [[], []]

        for it1, ts1, a1, lgn1 in cases :

            label_list[0].append('{0:<4},{1:<4}'.format(ts1, it1))
            label_list[1].append('{0:>4},{1:>4}'.format(ts1, it1))

            if len(alphas) > 1 :
                label_list[0][-1] += (',{0:3.2}'.format(a1))
                label_list[1][-1] += (',{0:3.2}'.format(a1))

            if len(LGNs) > 1 :
                label_list[0][-1] += (',{0}'.format(lgn1[0].upper()))
                label_list[1][-1] += (',{0}'.format(lgn1[0].upper()))

            # print it1, ts1
            for it2, ts2, a2, lgn2 in cases :
                # print '\t' + str((it2, ts2))
                sim = jaccard_similarity(rankings[it1][ts1][a1][lgn1], rankings[it2][ts2][a2][lgn2])
                similarity[((it1, ts1, a1, lgn1), (it2, ts2, a2, lgn2))] = sim

        result_matrix = []

        for i1, ts1, a1, lgn1 in cases :
            result_matrix.append([])
            for i2, ts2, a2, lgn2 in cases :
                result_matrix[-1].append(similarity[((i1, ts1, a1, lgn1), (i2, ts2, a2, lgn2))])

        a_file = open(temp_file, 'wb')
        pickle.dump((result_matrix, label_list, precision), a_file)
        a_file.close()
    else :
        a_file = open(temp_file, 'rb')
        result_matrix, label_list, precision = pickle.load(a_file)
        a_file.close()

    return result_matrix, label_list, precision


def get_LGN_probes(lgn_path) :
    """
    :type lgn_path : str
    :rtype : set
    """
    lgn = []

    with open(lgn_path) as f :
        for row in f.readlines()[1 :] :
            lgn += row.replace('\n', '').split(',')

    return set(lgn)


def get_relation_classes(class_file) :
    """
    :type lgn_path : str
    :rtype  : dict
    """
    relation_classes = {}

    with open(class_file, 'r') as f :
        for line in f.readlines() :
            probe, relation_class = tuple(map(lambda s : s.strip(), line.split(',')))
            relation_classes[probe] = relation_class

    return relation_classes


def get_file_infos(folder_path, files) :
    """
    :type folder_path : str
    :type files : list
    :rtype  : list
    """
    infos = []
    for x in files :
        file_path = folder_path + x
        with open(file_path) as f :
            line = f.readline()
            try :
                _, lgn, alpha, it, ts = tuple(map(lambda s : s.strip(), line.split(' ')))
            except Exception as x1 :
                try :
                    _, lgn, it, ts = tuple(map(lambda s : s.strip(), line.split(' ')))
                    alpha = 0.05
                except Exception as x2 :
                    sys.stderr.write('[ERR] Unknown format for input files\n')
                    sys.exit(2)
        infos += [(file_path, lgn, float(alpha), int(it), int(ts))]
    return infos


def usage() :
    print "PARAMS :"
    print "\t-f\tThe path to the folder containing the files to analyze.\n"
    print "\t-l\tThe path to the LGN file.\n"
    print "\t-c\tThe path to the file containing the relation classes.\n"
    print "\t-t\tThe range, starting from 1 of which you want to plot the data.\n"
    print "\t-w\tThe path to the working directory. Optional, the default is\n\t\tthe one where the script has been launched.\n"
    print "\t-p\tThe prefix for the similarity files. Optional, the default is 'similarity'.\n"
    print "\t-n\tThe number of iteration for plotting the precision graph.\n"
    print "\t-x\tScatter plot mode (can be 'alpha' or 'empty').\n"
    print "\t-s\tFlag for skipping the plot of the similarities.\n"
    print "\t-g\tFlag for greyscale precision/scatter plots.\n"


def main() :
    try :
        opts, args = getopt.getopt(sys.argv[1 :], "f:l:w:c:t:p:n:x:sg")
    except getopt.GetoptError as err :
        sys.stderr.write(str(err) + '\n')
        usage()
        sys.exit(2)

    working_directory = folder_path = lgn_file = prefix = class_file = None
    expansion = True
    skip_similarities = False
    max_value = 1
    plotting_iteration_number = 2000
    scatterplot_mode = None
    colors_flag = True

    for o, v in opts :
        if o == "-f" :
            folder_path = v
        elif o == "-l" :
            lgn_file = v
        elif o == "-c" :
            class_file = v
        elif o == "-w" :
            working_directory = v
        elif o == "-p" :
            prefix = v
        elif o == "-t" :
            max_value = int(v)
        elif o == "-n" :
            plotting_iteration_number = int(v)
        elif o == "-x" :
            scatterplot_mode = v
        elif o == "-s" :
            skip_similarities = True
        elif o == "-g" :
            colors_flag = False
        else :
            assert False, "Unhandled option"

    if None in [folder_path, lgn_file, class_file] :
        sys.stderr.write('[ERR] The options -f, -l, -c must be specified\n')
        usage()
        sys.exit(2)

    try :
        # lista dei file .expansion
        ext = 'expansion' if expansion else 'interaction'
        files = [f.replace(folder_path, '', 1).strip() for f in iglob(folder_path+"*."+ext)]
    except :
        sys.stderr.write('[ERR] The folder doesn\'t exist\n')
        sys.exit(2)

    if not files:
        sys.stderr.write('[ERR] The folder doesn\'t contain files\n')
        sys.exit(2)

    if prefix is None :
        prefix = 'similarity'

    if working_directory is None :
        working_directory = getcwd() + '/'

    relation_classes = get_relation_classes(class_file)
    infos = get_file_infos(folder_path, files)

    output_file = working_directory + prefix + '_precision'
    plot_precision(infos, relation_classes, max_value, plotting_iteration_number, output_file, colors_flag=colors_flag)

    if scatterplot_mode :
        output_file = working_directory + prefix #extension will be added
        plot_scatter(infos, relation_classes, max_value, scatterplot_mode, output_file, colors_flag=colors_flag)

    if not skip_similarities :
        recompute_policy = ['']

        for top in range(1, max_value + 1) :
            output_file =  working_directory + prefix + '_top_' + str(top)
            result_matrix, label_list, correctness = parse_data(top, infos, relation_classes, working_directory, prefix, recompute_policy)
            plot_similarity(result_matrix, label_list, correctness, top, output_file)


if __name__ == '__main__' :
    main()
