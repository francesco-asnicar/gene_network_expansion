#!/bin/bash


today=$(date +"%Y%m%d%H%M")

if [ -f "running" ]; then
    echo "Another instance already running!" | tee logs/${today}_experiments_download_and_postprocess.log
    exit
fi

touch running

# update db files
echo "Updating db files" | tee logs/${today}_experiments_download_and_postprocess.log
ssh gene "mysql -u ugene -pdb@G3ne gene -B -e \"SELECT * FROM experiments;\"" | sed 's/\t/,/g' >  db_gene_tbl_experiments.csv
ssh gene "mysql -u ugene -pdb@G3ne gene -B -e \"SELECT * FROM pcim WHERE executed=1;\"" | sed 's/\t/,/g' >  db_gene_tbl_pcim.csv
ssh gene "mysql -u ugene -pdb@G3ne gene -B -e \"SELECT * FROM pcim_experiments;\"" | sed 's/\t/,/g' >  db_gene_tbl_pcim_experiments.csv

# get experiments results in gDrive
echo "Getting results list from Google Drive (unitn.it)" | tee -a logs/${today}_experiments_download_and_postprocess.log
/home/francesco/bin/rclone --quiet ls "gDrive unitn":experiments_results/ | rev | cut -f1 -d' ' | rev | cut -f1 -d'.' | sort | uniq > results_list_gdrive.txt

# get experiments list in gDrive
echo "Getting experiments list from Google Drive (unitn.it)" | tee -a logs/${today}_experiments_download_and_postprocess.log
/home/francesco/bin/rclone --quiet ls "gDrive unitn":experiments_raw_data/ | rev | cut -f1 -d' ' | rev | cut -f1 -d'.' | sort | uniq > experiments_list_gdrive.txt

# compute the local missing experiments
comm -23 experiments_list_gdrive.txt results_list_gdrive.txt > experiments_to_download_and_postprocess.txt

n=`cat experiments_to_download_and_postprocess.txt | wc -l`

#if [[ $n -ge 5000 ]]; then
#    echo "Something is wrong! There are ${n} to download!!" | tee logs/${today}_experiments_download_and_postprocess.log
#    rm running
#    exit
#fi

echo "There are ${n} experiments to download and to post-process!" | tee -a logs/${today}_experiments_download_and_postprocess.log

# download the experiments
cd /data/downloads/

for i in $(cat ../experiments_to_download_and_postprocess.txt); do
	echo "    Downloading ${i}" | tee -a ../logs/${today}_experiments_download_and_postprocess.log
	/home/francesco/bin/rclone --quiet copy "gDrive unitn":experiments_raw_data/${i}.tar ./ && tar -xf ${i}.tar
done

cd /data/

# post-process the downloaded experiments
python process.py experiments_to_download_and_postprocess.txt 2>&1 | tee -a logs/${today}_experiments_download_and_postprocess.log

# remove experiments data folders successfully analyzed
for i in $(cat experiments_to_download_and_postprocess.txt); do
	if [ -f /data/results/${i}.expansion ] && [ -f /data/results/${i}.interactions ]; then
		echo "${i}" 2>&1 | tee -a logs/${today}_experiments_download_and_postprocess.log
		rm -fr /data/downloads/${i}* # remove the ${i}.tar (if not alredy removed) and the ${i} folder
	fi
done

echo 2>&1 | tee -a logs/${today}_experiments_download_and_postprocess.log

# backup newly computed results to gDrive
/home/francesco/bin/rclone copy /data/results "gDrive unitn":experiments_results/ 2>&1 | tee -a logs/${today}_experiments_download_and_postprocess.log

# move files used in the process
cd experiments_download_and_postprocess_files/
./mv.sh ${today}
cd ..

rm running
