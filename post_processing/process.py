#!/usr/bin/env python



import os
import sys
import math
import time
import subprocess as sb
import postprocessing as pp


comp = "/home/francesco/bin/pigz"
uncomp = "/home/francesco/bin/unpigz"

experiments_to_process = [line.strip().split('_')[0] for line in open(sys.argv[1])]
experiments_files = dict([(line.strip().split(',')[0], line.strip().split(',')[1:]) for line in open("db_gene_tbl_experiments.csv")])
experiments = dict([(line.strip().split(',')[0], line.strip().split(',')[1:]) for line in open("db_gene_tbl_pcim.csv") if line.strip().split(',')[0] in experiments_to_process])
experiments2file = dict([(line.strip().split(',')[0], line.strip().split(',')[1]) for line in open("db_gene_tbl_pcim_experiments.csv") if line.strip().split(',')[0] in experiments_to_process])


at_fos_classes = "at_fos_classes.csv"
at_flv_classes = "at_flavonoids_classes.csv"
ec_classes = "ec_classes_empty.csv"
pa_classes = "pa_classes_empty.csv"
sc_classes = "sc_classes_empty.csv"
vv_classes = "vv_classes_empty.csv"
hs_classes = "hs_classes_empty.csv"

at_fos = "/home/francesco/bitbucket/gene_network_expansion/data/athaliana/fos/"
at_flv = "/home/francesco/bitbucket/gene_network_expansion/data/athaliana/flavonoids/"
ec = "/home/francesco/bitbucket/gene_network_expansion/data/ecoli/"
sc = "/home/francesco/bitbucket/gene_network_expansion/data/scerevisiae/"
pa = "/home/francesco/bitbucket/gene_network_expansion/data/paeruginosa/"
vv = "/home/francesco/bitbucket/gene_network_expansion/data/vvinifera/"
hs = "/home/francesco/bitbucket/gene_network_expansion/data/hsapiens/"

to_process = []
i = 0
sleeping = False
backoff = 50
backoff_reset = 50
backoff_step = 5
backoff_limit = 120

while i < len(experiments_to_process):
    exp = experiments_to_process[i]
    lgn_file = None
    exp_file = None
    cls_file = None
    base_path = None
    organism = None
    org = None
    descr = None
    print_exp = False

    if exp in experiments:
        lgn_file = experiments[exp][2]
        lgn_file = lgn_file[lgn_file.rfind('/')+1:]

        org = experiments[exp][0]
        descr = experiments[exp][1]
    else:
        pp.printstd('['+exp+'] No local gene network file found!\n')
        i += 1
        continue

    if exp in experiments2file:
        exp_file = experiments_files[experiments2file[exp]][0]
    else:
        pp.printstd('['+exp+'] No experiments file found!\n')
        i += 1
        continue

    if org in 'At': # Arabidopsis thaliana
        organism = 'At'

        if ('fos' in descr.lower()) or ('fos' in exp_file.lower()):
            cls_file = at_fos_classes
            base_path = at_fos
        else:
            cls_file = at_flv_classes
            base_path = at_flv
    elif org in 'Ec': # Escherichia coli
        organism = 'Ec'
        cls_file = ec_classes
        base_path = ec
    elif org in 'Pa': # Pseudomonas aeruginosa
        organism = 'Pa'
        cls_file = pa_classes
        base_path = pa
    elif org in 'Sc': # Saccharomyces cerevisiae
        organism = 'Sc'
        cls_file = sc_classes
        base_path = sc
    elif org in 'Vv': # Vitis vinifera
        organism = 'Vv'
        cls_file = vv_classes
        base_path = vv
    elif org in 'Hs': # Homo sapiens
        organism = 'Hs'
        cls_file = hs_classes
        base_path = hs

    if not os.path.isfile(base_path+exp_file):
        if not sleeping:
            pp.printstd(exp+'\n')
            pp.printstd('    Downloading EXP '+experiments_files[experiments2file[exp]][1]+'\n')
            print_exp = True

        try:
            sb.check_call(["scp", "-Cq", "gene:"+experiments_files[experiments2file[exp]][1], base_path+exp_file])
            sleeping = False
            backoff = backoff_reset
        except:
            sleeping = True
            pp.printstd('    scp failed, retrying in '+str(backoff)+'s\n')
            time.sleep(backoff)
            backoff += backoff_step

            if backoff > backoff_limit:
                backoff = backoff_reset

            continue

    if not os.path.isfile(base_path+lgn_file):
        if (not print_exp) and (not sleeping):
            pp.printstd(exp+'\n')
            pp.printstd('    Downloading LGN '+experiments[exp][2]+'\n')
            print_exp = True

        try:
            sb.check_call(["scp", "-Cq", "gene:"+experiments[exp][2], base_path+lgn_file])
            sleeping = False
            backoff = backoff_reset
        except:
            sleeping = True
            pp.printstd('    scp failed, retrying in '+str(backoff)+'s\n')
            time.sleep(backoff)
            backoff += backoff_step

            if backoff > backoff_limit:
                backoff = backoff_reset

            continue

    if not organism:
        if not print_exp:
            pp.printstd(exp+'\n')
            print_exp = True

        pp.printstd('    No species found! Are we analysing an alien??\n')
    elif not os.path.isfile(base_path+exp_file):
        if not print_exp:
            pp.printstd(exp+'\n')
            print_exp = True

        pp.printstd('    File'+base_path+exp_file+' not found!\n')
    elif not os.path.isfile(base_path+lgn_file):
        if not print_exp:
            pp.printstd(exp+'\n')
            print_exp = True

        pp.printstd('    File '+base_path+lgn_file+' not found!\n')
    elif (os.path.isfile(base_path+exp_file)) and (os.path.isfile(base_path+lgn_file)) and (os.path.isfile(base_path+cls_file)) and (organism):
        to_process.append([exp+'_'+organism, "downloads/", base_path+lgn_file, base_path+exp_file,  base_path+cls_file, "results/", comp, uncomp])
    else:
        if not print_exp:
            pp.printstd(exp+'\n')
            print_exp = True

        pp.printstd("    Something very bad happened!\n")

    i += 1

pp.printstd("There are "+str(len(to_process))+" experiments to process!\n")

chunk = int(math.ceil(len(to_process)/30.0))
pp.postpro(to_process, 3, chunk if chunk > 1 else 1)
