#!/usr/bin/env python


import os
import math
import parserBoinc
from sys import stdout, stderr, exit
from glob import iglob
from argparse import ArgumentParser
from subprocess import call
from multiprocessing import Pool


def printstd(msg):
    stdout.write(msg)
    stdout.flush()


def printerr(msg):
    stderr.write(msg)
    stderr.flush()


def error(fun, msg, kill=True):
    printerr("[ERR "+fun+"] "+msg+"\n")
    if kill: exit(1)


def info(fun, msg):
    printstd("[NFO "+fun+"] "+msg+"\n")


def preflight_check(progs):
    for prog in progs:
        try:
            with open(os.devnull, 'w') as devnull:
                call([prog, '-h'], stdout=devnull, stderr=devnull)
        except OSError:
            error("preflight_check", "OSError: "+prog+" not found!")
        except:
            error("preflight_check", "Unexpected error while testing "+prog+"!")


def read_params():
    parser = ArgumentParser(description="")
    parser.add_argument('-i', '--ids', type=str, required=True,
        help="Specified the PC-IM ids to post-process. Can be either a file or a csv list.")
    parser.add_argument('-p', '--pcim', type=str, required=True,
        help="Is the path to the folder that contain the PC-IMs.")
    parser.add_argument('-l', '--lgn', type=str, required=True,
        help="The path to the local gene network file.")
    parser.add_argument('-e', '--experiments', type=str, required=True,
        help="The path to the experiments/expressions data file")
    parser.add_argument('-c', '--classes', type=str, required=True,
        help="The path to the file with the classification of the genes.")
    parser.add_argument('-o', '--output', type=str, required=True,
        help='Specify the output direcotry')
    parser.add_argument('--ncpu', type=int, default=1,
        help="Define the number of core to used.")
    parser.add_argument('--compress', type=str, default="gzip",
        help="Define the software to used for compressin. Default gzip.")
    parser.add_argument('--uncompress', type=str, default="gunzip",
        help="Define the software to used for uncompressing. Defaul gunzip.")
    args = parser.parse_args()

    # check that pcim_path end with '/'
    if not args.pcim.endswith("/"):
        info("read_params", "Added missing '/' at the end of the -p/--pcim param")
        args.pcim += "/"

    # check that pcim_path is a valid directory
    if not os.path.isdir(args.pcim):
        error("read_params", "'"+args.pcim+"' is not a valid path or directory")

    # check that lgn_path is a valid file
    if not os.path.isfile(args.lgn):
        error("read_params", "'"+ args.lgn+"' is not a valid file")

    # check that experiments_path is a valid file
    if not os.path.isfile(args.experiments):
        error("read_params", "'"+args.experiments+"' is not a valid file")

    # check that classes_path is a valid file
    if not os.path.isfile(args.classes):
        error("read_params", "'"+args.classes+"' is not a valid file")

    # check that output ends with '/'
    if not args.output.endswith('/'):
        info("read_params", "Added missing '/' at the end of the -o/--output param")
        args.output += '/'

    # check that output is a valid directory
    if not os.path.isdir(args.output):
        error("read_params", "'"+args.output+"'is not a valid path or directory")

    # check that ncpu is a positive value
    if args.ncpu <= 0:
        info("read_params", "Invalid ncpu value: "+args.ncpu+", reset to 1")
        args.ncpu = 1

    return args


def uncompress(prog, path):
    if not os.path.isdir(path):
        error(prog, "Invalid path: " + path)

    for f in iglob(path+"*.gz"):
        if call([prog, "--force", f]):
            info(prog, "Cannot decompress: "+f)

    return True


def compress(prog, path):
    if not os.path.isdir(path):
        error(prog, "Invalid path: " + path)

    for f in iglob(path+"*"):
        if call([prog, f]):
            info(prog, "Cannot compress: "+f)

    return True


def parserBoinc_wrapper(pcim_id, execution_path, lgn, experiments, classes, output_path):
    if not os.path.isdir(execution_path+pcim_id+"/results/"):
        error("parserBoinc_wrapper", "Invalid results path: "+execution_path+pcim_id+"/results/", kill=False)
        return False

    if not os.path.isfile(execution_path+pcim_id+"/parameters.txt"):
        error("parserBoinc_wrapper", "Invalid parameters path: "+execution_path+pcim_id+"/parameters.txt", kill=False)
        return False

    if not os.path.isfile(execution_path+pcim_id+"/frequency.txt"):
        error("parserBoinc_wrapper", "Invalid frequency path: "+execution_path+pcim_id+"/frequency.txt", kill=False)
        return False

    if not os.path.isfile(lgn):
        error("parserBoinc_wrapper", "Invalid lgn path: "+lgn, kill=False)
        return False

    if not os.path.isfile(experiments):
        error("parserBoinc_wrapper", "Invalid experiments path: "+experiments, kill=False)
        return False

    if not os.path.isfile(classes):
        error("parserBoinc_wrapper", "Invalid classes path: "+classes, kill=False)
        return False

    parserBoinc.boincPost(execution_path+pcim_id+"/results/", execution_path+pcim_id+"/parameters.txt", execution_path+pcim_id+"/frequency.txt", lgn, experiments, classes, output_path+pcim_id)
    return True


def postpro_exe(params):
    if len(params) != 8:
        error("postpro_exe", "Invalid number of parameters: "+str(len(params)), kill=False)
        return False

    try:
        pcim_id = params[0]
        pcim_path = params[1]
        lgn = params[2]
        experiments = params[3]
        classes = params[4]
        results = pcim_path + pcim_id + "/results/"
        output_path = params[5]
        comp = params[6]
        uncomp = params[7]

        if not os.path.isdir(pcim_path):
            error("postpro_exe", "Invalid path: "+pcim_path+pcim_id, kill=False)
            return False

        # decompress results folder
        printstd("["+pcim_id+"] Decompressing\n")

        if not uncompress(uncomp, results):
            error("postpro_exe", "Gunzip failed "+pcim_id, kill=False)
            return False

        # post-processing
        printstd("["+pcim_id+"] Post-processing\n")
        if not parserBoinc_wrapper(pcim_id, pcim_path, lgn, experiments, classes, output_path):
            error("postpro_exe", "parserBoinc_wrapper failed "+pcim_id, kill=False)
            return False

        # # compress results folder
        # printstd("["+pcim_id+"] Compressing\n")
        # if not compress(comp, results):
        #     error("postpro_exe", "Gzip failed "+pcim_id, kill=False)
        #     return False
    except OSError:
        error("postpro_exe", "OSError", kill=False)
        return False
    except KeyboardInterrupt:
        error("postpro_exe", "KeyboardInterrupt", kill=False)
        return False
    except:
        error("postpro_exe", "Unexpected error!", kill=False)
        return False

    return True


def postpro(cmds, ncpu, chunk):
    pool = Pool(processes=ncpu)
    pool.imap_unordered(postpro_exe, cmds, chunksize=chunk)
    pool.close()
    pool.join()


if __name__ == '__main__':
    args = read_params()
    pcim_ids = []

    preflight_check([args.compress, args.uncompress])

    if os.path.isfile(args.ids): # PC-IM ids are on a file
        pcim_ids = [r.strip() for r in open(args.ids)]
    else: # interpret them as a csv list
        pcim_ids = [s.strip() for s in args.ids.split(',')]

    cmds = [[pcim_id, args.pcim, args.lgn, args.experiments, args.classes, args.output, args.compress, args.uncompress] for pcim_id in pcim_ids]
    chunk = int(math.ceil(len(cmds)/(args.ncpu*10.0)))
    postpro(cmds, args.ncpu, chunk if chunk > 1 else 1)
