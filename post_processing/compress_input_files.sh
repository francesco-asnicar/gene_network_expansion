#!/bin/bash


# check whether the experiment has some uncompressed files, either input or results, and if it is the case compress them

cd /data/downloads/

for i in $(ls -d *_*/); do
    cd ${i}

    unc=`ls *wu*.txt 2>/dev/null | wc -l`

    if [[ $unc -gt 0 ]]; then
        echo "Compressing input ${i::-1}"
        # gzip *wu*.txt
        pigz *wu*.txt
    fi

    if [ -d "results/" ]; then
        cd results/

        unc=`ls *wu* 2>/dev/null | grep -v ".gz$" | wc -l`

        if [[ $unc -gt 0 ]]; then
            echo "Compressing results ${i::-1}"
            # gzip `ls *wu* 2>/dev/null | grep -v ".gz$"`
            pigz `ls *wu* 2>/dev/null | grep -v ".gz$"`
        fi

        cd ../
    fi

    cd ../
done
