
class ClassFileException(Exception) :
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


def readClassFile(path) :
    try :
        f = open(path,'r')
    except IOError :
        raise ClassFileException("Unable to open classfile: " + path)
    classes = {}
    for line in f :
        gene,class_annotation = line.strip().split(',')
        if gene not in classes :
            classes[gene] = class_annotation
        else :
            raise ClassFileException("Duplicate annotation in classfile: " + path)

    return classes

def writeClassFile(path,classes) :
    dump = '\n'.join(map(lambda t : t[0]+','+t[1],classes.items())) + '\n'
    with open(path,'w') as f :
        f.write(dump)

def updateClasses(classes,classes_update,verbose=False,mode='ask') :
    len_before = len(classes)
    for k,v in classes_update.items() :
        if k not in classes :
            if verbose : print "ADDED: {},{}".format(k,v)
            classes[k] = v
        elif classes[k] != v :
            if mode == 'preserve' :
                if verbose : print "UPDATE IGNORED: {},({} / {})".format(k,classes[k],v)
            elif mode == 'update' :
                if verbose : print "UPDATED {} : {} -> {}".format(k,classes[k],v)
                classes[k] = v
            elif mode == 'ask' :
                if raw_input("UPDATE {} : {} -> {} ? [Y/N] ".format(k,classes[k],v)).upper().startswith('Y') :
                    if verbose : print "UPDATED {} : {} -> {}".format(k,classes[k],v)
                    classes[k] = v
                else :
                    if verbose : print "UPDATE IGNORED: {},({} / {})".format(k,classes[k],v)
    if verbose :
        print "SIZE before/after update: {}/{}".format(str(len_before),str(len(classes)))

    return classes

def diffClasses(classes1,classes2) :
    classes12 = set(classes1.keys()).intersection(set(classes2.keys()))
    print "SIZES:"
    print "CLASSES 1\tINTERSECTION\tCLASSES 2"
    print "{}\t{}\t{}".format(str(len(classes1)),str(len(classes12)),str(len(classes2)))
    print "\nDIFF:"
    for k in classes12 :
        if classes1[k] != classes2[k] : print '\t'.join([k,classes1[k],classes2[k]])

if __name__ == '__main__' :
    from sys import argv
    if len(argv) - 1 == 4 and argv[3].lower() in ['ask','update','preserve'] :
        c1 = readClassFile(argv[1])
        c2 = readClassFile(argv[2])
        c3 = updateClasses(c1,c2,mode = argv[3].lower(),verbose = True)
        writeClassFile(argv[4],c3)
    elif len(argv) - 1 == 3 and argv[3].lower() == 'diff' :
        c1 = readClassFile(argv[1])
        c2 = readClassFile(argv[2])
        diffClasses(c1,c2)
    else :
        print "Usage:"
        print "  $ python updateClasses.py <base_classes_file> <update_classes_file> <mode> <output_file>"
        print "where <mode> can be:"
        print "  ask: interactively asks the user whether to update or not"
        print "  preserve: in case of name clash, preserves the base_file version"
        print "  update: in case of name clash, updates with the update_file version"
        print
        print "Alternatively it can be used to print the differences between two files of classes"
        print "  $ python updateClasses.py <class_file_1> <class_file_2> diff"
