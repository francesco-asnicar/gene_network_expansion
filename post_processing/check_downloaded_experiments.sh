#!/bin/bash


# check whether the number of input workunits is equal to the number of results (.gz)
# if no local results are present, check if they are available on the cnr server. If there are, download them!
# print the ids of the experiments that have different values

> check_downloaded_experiments.txt

# fuck tmp folder on loa!
for i in $(ls -d downloads/*/ | grep -v "tmp"); do
    id=`echo ${i} | cut -f2 -d'/'`
    wu=`ls ${i}*wu* 2>/dev/null | wc -l`
    re=`ls ${i}results/*.gz 2>/dev/null | wc -l`

    if [[ $re -eq 0 ]]; then
        loa_re=`ssh cnr "ls drop/${id}/results/*" 2>/dev/null | wc -l`

        if [[ $wu -eq $loa_re ]]; then
            echo "    Downloading missing results for ${id}"
            scp -Cqr cnr:drop/${id}/results/* ${i}/results/
            re=`ls ${i}results/*.gz 2>/dev/null | wc -l`
        fi
    fi

    if [[ $wu -ne $re ]]; then
        echo ${id} >> check_downloaded_experiments.txt
    fi
done
