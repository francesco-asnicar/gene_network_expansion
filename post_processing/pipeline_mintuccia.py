#!/usr/bin/python
import logging as l
import os
import shutil
import subprocess as sp
import sys
import traceback
import multiprocessing as mp
from datetime import datetime
from threading import Thread
from time import sleep

import postprocessing as pp

comp = "gunzip"
uncomp = "gunzip"

drive_alias = 'unitn_drive'
base_fld = '/home/boinc_utils/BOINC_PP'
download_fld = os.path.join(base_fld, 'downloads')
log_fld = os.path.join(base_fld, 'logs')
result_fld = os.path.join(base_fld, 'results')
lgn_data_fld = '/home/boinc_utils/gene_network_expansion/data/'

max_attempts = 10
n_worker = 3


def setup_logger():
    today = '{0:%Y%m%d%H%M}'.format(datetime.now())
    root = l.getLogger()
    root.setLevel(l.INFO)
    formatter = l.Formatter(fmt='%(asctime)s - %(levelname)s - %(message)s', datefmt='%Y-%m-%d,%H:%M:%S')

    fl = l.FileHandler(filename=os.path.join(log_fld, '{}_experiments_download_and_postprocess.log'.format(today)))
    fl.setLevel(l.INFO)
    fl.setFormatter(formatter)

    ch = l.StreamHandler(sys.stdout)
    ch.setLevel(l.INFO)
    ch.setFormatter(formatter)

    root.addHandler(fl)
    root.addHandler(ch)

    l.debug('Level set to DEBUG')


def setup_experiments_maps():
    experiments = dict()
    experiments['input_files'] = dict([(line.strip().split(',')[0], line.strip().split(',')[1:]) for line in open("db_gene_tbl_experiments.csv")])
    experiments['pcim_params'] = dict([(line.strip().split(',')[0], line.strip().split(',')[1:]) for line in open("db_gene_tbl_pcim.csv")])
    experiments['input_to_params'] = dict([tuple(line.strip().split(',')) for line in open("db_gene_tbl_pcim_experiments.csv")])

    base_paths = {
        'At_fos': os.path.join(lgn_data_fld, "athaliana/fos/"),
        'At_flv': os.path.join(lgn_data_fld, "athaliana/flavonoids/"),
        'Ec': os.path.join(lgn_data_fld, "ecoli/"),
        'Pa': os.path.join(lgn_data_fld, "scerevisiae/"),
        'Sc': os.path.join(lgn_data_fld, "paeruginosa/"),
        'Vv': os.path.join(lgn_data_fld, "vvinifera/")
    }

    return experiments, base_paths


def check_and_fix_files(pcim_id, proc_id):
    pcim_id_ = pcim_id.split('_')[0]
    log_offset = '[PROC   {}] '.format(proc_id)
    l.debug('{} checking PCIM {} files'.format(log_offset, pcim_id))
    if pcim_id_ in experiments['pcim_params']:
        lgn_file = experiments['pcim_params'][pcim_id_][2].split('/')[-1]

        org = experiments['pcim_params'][pcim_id_][0]
        descr = experiments['pcim_params'][pcim_id_][1]
    else:
        l.error('{}[{}] No local gene network file found!'.format(log_offset, pcim_id_))
        return None

    if pcim_id_ in experiments['input_to_params']:
        exp_id = experiments['input_to_params'][pcim_id_]
        exp_file = experiments['input_files'][exp_id][0]
    else:
        l.error('{}[{}] No experiments file found!'.format(log_offset, pcim_id_))
        return None

    key = org if org != 'At' else '{}_{}'.format(org, 'fos' if 'fos' in descr.lower() else 'flv')

    try:
        organism_base_path = base_paths[key]
    except:
        l.error('{}Unknown organism identifier {}'.format(log_offset, key))
        return None

    if not os.path.isfile(organism_base_path + lgn_file):
        lgn_path = experiments['pcim_params'][pcim_id_][2]
        l.info('{}Downloading LGN {}'.format(log_offset, lgn_path.split('/')[-1]))
        attempts = 0
        downloaded = max_attempts_reached = False
        while not downloaded and not max_attempts_reached:
            try:
                sp.check_call(["scp", "-Cq", "gene:" + lgn_path, organism_base_path + lgn_file])
                downloaded = True
            except:
                if attempts > max_attempts:
                    max_attempts_reached = True
                else:
                    attempts += 1
                    sleep(10)
        if not downloaded:
            l.error('{}Unable to download LGN {}'.format(log_offset, lgn_path.split('/')[-1]))
            return None

    if not os.path.isfile(organism_base_path + exp_file):
        exp_path = experiments['input_files'][exp_id][2]
        l.info('{}Downloading EXP {}'.format(log_offset, exp_file))
        attempts = 0
        downloaded = max_attempts_reached = False
        while not downloaded and not max_attempts_reached:
            try:
                sp.check_call(["scp", "-Cq", "gene:" + exp_path, organism_base_path + exp_file])
                downloaded = True
            except:
                if attempts > max_attempts:
                    max_attempts_reached = True
                else:
                    attempts += 1
                    sleep(10)
        if not downloaded:
            l.error('{}Unable to download EXP {}'.format(log_offset, exp_file))
            return None

    return org, organism_base_path, lgn_file, exp_file


def update_db_files():
    l.info('[CHECKER ] Updating db files')
    o1 = sp.check_call('ssh gene "mysql -u ugene -pdb@G3ne gene -B -e \\"SELECT * FROM experiments;\\"" | sed \'s/\t/,/g\' >  db_gene_tbl_experiments.csv', shell=True)
    o2 = sp.check_call('ssh gene "mysql -u ugene -pdb@G3ne gene -B -e \\"SELECT * FROM pcim WHERE executed=1;\\"" | sed \'s/\t/,/g\' >  db_gene_tbl_pcim.csv', shell=True)
    o3 = sp.check_call('ssh gene "mysql -u ugene -pdb@G3ne gene -B -e \\"SELECT * FROM pcim_experiments;\\"" | sed \'s/\t/,/g\' >  db_gene_tbl_pcim_experiments.csv', shell=True)
    try:
        assert o1 + o2 + o3 == 0
    except AssertionError:
        l.error('[CHECKER ] Unable to update DB files')
        sys.exit(1)


def update_unprocessed_experiments(to_process_queue, considered):
    l.info('[CHECKER ] Getting raw data list from Google Drive (unitn.it)')
    o1 = sp.check_call('rclone ls {}:experiments_raw_data/ | rev | cut -f1 -d\' \' | rev | cut -f1 -d\'.\' | sort | uniq > raw_experiments_list.txt'.format(drive_alias), shell=True)
    l.info('[CHECKER ] Getting processed list from Google Drive (unitn.it)')
    o2 = sp.check_call('rclone ls {}:experiments_results/ | rev | cut -f1 -d\' \' | rev | cut -f1 -d\'.\' | sort | uniq > processed_experiments_list.txt'.format(drive_alias), shell=True)
    o3 = sp.check_call('comm -23 raw_experiments_list.txt processed_experiments_list.txt > experiments_to_download_and_process.txt', shell=True)

    try:
        assert o1 + o2 + o3 == 0
    except AssertionError:
        l.error('[CHECKER ] Unable to compute unprocessed experiments')
        return

    new_experiments = 0
    with open('experiments_to_download_and_process.txt') as f:
        for line in f:
            pcim_id = line.strip()
            if pcim_id not in considered:
                to_process_queue.put(pcim_id)
                considered.add(pcim_id)
                new_experiments += 1

    l.info('[CHECKER ] {} new PCIMs have been added to the work queue'.format(new_experiments))
    l.info('[CHECKER ] The queue at the moment contains {} PCIM to be processed'.format(to_process_queue.qsize()))


def post_process_worker(worker_id):
    global to_process
    global processed

    l.debug('[PROC   {}] started'.format(worker_id))

    while True:
        try:
            pcim_id = to_process.get()
            to_process.task_done()

            os.chdir(download_fld)
            l.info('[PROC   {}] Downloading {}'.format(worker_id, pcim_id))
            sp.check_call('rclone --quiet copy {1}:experiments_raw_data/{0}.tar ./ && tar -xf {0}.tar && rm {0}.tar'.format(pcim_id, drive_alias), shell=True)
            os.chdir(base_fld)
            ret_val = check_and_fix_files(pcim_id=pcim_id, proc_id=worker_id)
            if ret_val is not None:
                org, organism_base_path, lgn_file, exp_file = ret_val
            else:
                l.error('[PROC   {}] Something went wrong, {} will be skipped.'.format(worker_id, pcim_id))
                to_process.put(pcim_id)
                continue

            l.info('[PROC   {}] Starting postprocessing of {}'.format(worker_id, pcim_id))
            ret_val = pp.postpro_exe([pcim_id, "downloads/", organism_base_path + lgn_file, organism_base_path + exp_file, 'running', "results/", comp, uncomp])
            if ret_val:
                l.info('[PROC   {}] Postprocessing of {} completed successfully'.format(worker_id, pcim_id))
                processed.put(pcim_id)
            else:
                l.error('[PROC   {}] Something went wrong in the postprocessing, {} will be skipped.'.format(worker_id, pcim_id))
                to_process.put(pcim_id)
        except:
            traceback.print_exc(file=l.StreamHandler().stream)
        finally:
            try:
                os.remove(os.path.join(base_paths, download_fld, pcim_id + '.tar'))
            except OSError:
                pass
            try:
                shutil.rmtree(os.path.join(base_paths, download_fld, pcim_id))
            except OSError:
                pass


class WorkCheckerThread(Thread):
    def run(self):
        global to_process
        global considered
        while True:
            update_db_files()
            update_unprocessed_experiments(to_process, considered)
            sleep(7200)


class UploaderThread(Thread):
    def run(self):
        global processed
        to_upload = empty_results = []
        while True:
            to_upload.append(processed.get())
            if len(to_upload) > 50:
                l.info('[UPLOADER] Starting upload of the results of {} processed PCIM'.format(len(to_upload)))
                to_upload_files = 'to_upload_files'
                with open(to_upload_files, 'w') as up:
                    for pcim_id in to_upload:
                        if os.path.getsize(os.path.join(base_fld, result_fld, pcim_id+'.expansion')) == 59:
                            l.warning('[UPLOADER] {} has empty expansion, it won\'t be uploaded'.format(pcim_id))
                            empty_results.append(pcim_id)
                        else:
                            up.write('{0}.expansion\n{0}.interactions\n'.format(pcim_id))
                try:
                    sp.check_call(['rclone', '-q', 'copy', 'results', '{}:experiments_results'.format(drive_alias), '--files-from', to_upload_files])
                except:
                    l.warning('[UPLOADER] Upload failed')
                    continue
                for pcim_id in to_upload:
                    if pcim_id not in empty_results:
                        os.remove(os.path.join(result_fld, '{}.expansion'.format(pcim_id)))
                        os.remove(os.path.join(result_fld, '{}.interactions'.format(pcim_id)))
                l.info('[UPLOADER] Upload finished, waiting for new processed results')
                to_upload = []


if __name__ == '__main__':
    # Check if another istance is sunning
    if os.path.isfile('running'):
        l.error('Another instance already running!')
        sys.exit(1)

    open('running', 'w').close()

    try:
        setup_logger()
        experiments, base_paths = setup_experiments_maps()

        to_process = mp.JoinableQueue()
        processed = mp.JoinableQueue()
        considered = set()

        WorkCheckerThread().start()
        UploaderThread().start()

        pool = mp.Pool(processes=n_worker)
        pool.imap_unordered(post_process_worker, range(1, n_worker + 1))
        pool.close()

        pool.join()

    except Exception as e:
        traceback.print_exc(file=l.StreamHandler().stream)
