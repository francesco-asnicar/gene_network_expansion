#!/bin/bash


# check whether the experiment has the .expansion and .interactions file in the results/ folder
# print the ids of the experiments that do not have the result files

> check_processed_experiments.txt

for i in $(ls -d downloads/*/); do
    id=`echo ${i} | cut -f2 -d'/'`
    re=`ls results/${id}* 2>/dev/null | wc -l`

    if [[ $re -ne 2 ]]; then
        echo ${id} >> check_processed_experiments.txt
    fi
done
