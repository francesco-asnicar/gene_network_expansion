#!/usr/bin/env python


from os import path
from glob import glob

experiments = dict([( (line.strip().split(',')[0], line.strip().split(',')[1], line.strip().split(',')[2]), line.strip().split(',')[3].split('/')[-1]) for line in open("/data/experiments_download_and_postprocess_files/201701121300/db_gene_tbl_pcim.csv")])
experiments_files = dict([(line.strip().split(',')[0], line.strip().split(',')[1:]) for line in open("/data/experiments_download_and_postprocess_files/201701121300/db_gene_tbl_experiments.csv")])
experiments2file = dict([(line.strip().split(',')[0], line.strip().split(',')[1]) for line in open("/data/experiments_download_and_postprocess_files/201701121300/db_gene_tbl_pcim_experiments.csv")])

at_fos = "/home/francesco/bitbucket/gene_network_expansion/data/athaliana/fos/"
at_flv = "/home/francesco/bitbucket/gene_network_expansion/data/athaliana/flavonoids/"
ec = "/home/francesco/bitbucket/gene_network_expansion/data/ecoli/"
vv = "/home/francesco/bitbucket/gene_network_expansion/data/vvinifera/"

for idd, lgn in experiments.iteritems():
    num, org, descr = idd
    base_path = None

    if org == 'At':
        base_path = at_flv

        if ('fos' in descr.lower()) or ('fos' in experiments_files[experiments2file[num]][0]):
            base_path = at_fos
    elif org == 'Ec':
        base_path = ec
    elif org == 'Vv':
        base_path = vv
    else:
        print idd, lgn
        continue

    if path.isfile(base_path+lgn):
        exps = glob('/data/results/'+num+'_'+org+'*.expansion')

        for exp in exps:
            if path.isfile(exp):
                lgn_ids = set()

                with open(base_path+lgn) as f:
                    for row in f.readlines()[:1]:
                        lgn_ids |= set(row.strip().lower().split(','))

                with open(exp) as f:
                    for row in f.readlines()[2:]:
                        if row.strip().split(',')[1].lower() in lgn_ids:
                            print num+'_'+org, row.strip()
            else:
                print '[ERR] File', exp, 'not found'
    else:
        print '[ERR] File', base_path+lgn, 'not found'
