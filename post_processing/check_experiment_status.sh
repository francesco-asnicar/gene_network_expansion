#!/bin/bash


# check whether the experiment has the same number of input and result files
# print the ids of the experiments that have incongruent numbers
# print "<id>,inputs" if there are no input files
# print "<id>,results" if there are no result files
# print "<id>,uncompress" if the input and result files numbers do not match

> check_experiment_status.txt

for i in $(ls -d downloads/*/); do
    id=`echo ${i} | cut -f2 -d'/'`
    wu=`ls ${i}/*wu*txt 2>/dev/null | wc -l`
    re=`ls ${i}/results/*wu*gz 2>/dev/null | wc -l`

    if [[ $wu -eq 0 ]]; then
        echo "${id},inputs" >> check_experiment_status.txt
    elif [[ $re -eq 0 ]]; then
        echo "${id},results" >> check_experiment_status.txt
    elif [[ $wu -ne $re ]]; then
        echo "${id},uncompress" >> check_experiment_status.txt
    fi
done
