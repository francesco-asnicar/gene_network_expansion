#!/bin/bash


today=$(date +"%Y%m%d%H%M")

if [ -f "running" ]; then
    echo "Another instance already running!" | tee /data/logs/${today}_data_upload.log
    exit
fi

touch running

case "$1" in
    upload)
        echo "Uploading data/ to gDrive unitn:lgns_and_expression_data/" | tee /data/logs/${today}_data_upload.log
        /home/francesco/bin/rclone copy data/ "gDrive unitn":lgns_and_expression_data/ 2>&1 | tee /data/logs/${today}_data_upload.log
        ;;
    download)
        echo "Downloading to gDrive unitn:lgns_and_expression_data/ to data/" | tee /data/logs/${today}_data_upload.log
        /home/francesco/bin/rclone copy "gDrive unitn":lgns_and_expression_data/ data/ 2>&1 | tee /data/logs/${today}_data_upload.log
        ;;
    *)
        echo "Usage:" | tee /data/logs/${today}_data_upload.log
        echo "    ./data_update.sh [upload|download]" 2>&1 | tee /data/logs/${today}_data_upload.log
        ;;
esac

rm running
