#!/usr/bin/env python


import os
import time
import numpy as np
from sys import stdout, stderr, exit
from glob import iglob
from argparse import ArgumentParser


def printstd(msg):
    stdout.write(msg)
    stdout.flush()


def printerr(msg):
    stderr.write(msg)
    stderr.flush()


def error(fun, msg, kill=True):
    printerr("[ERR "+fun+"] "+msg+"\n")

    if kill:
        exit(1)


def info(fun, msg):
    printstd("[NFO "+fun+"] "+msg+"\n")


def read_params():
    parser = ArgumentParser(description="")
    parser.add_argument('-i', '--input', type=str, required=True,
        help="Specified the input folder that contains the .expansion files.")
    parser.add_argument('-o', '--output', type=str, required=True,
        help='Specify the output folder')
    parser.add_argument('--top', type=int, required=False, default=100,
        help="Specify to consider only the specified number of elements sorted by relative frequency (decreasing).")
    parser.add_argument('--threshold', type=float, required=False, default=0.9,
        help="Specify to consider only the elements with a relative frequency greater-or-equal to the number specified.")
    args = parser.parse_args()

    # check that pcim_path end with '/'
    if not args.input.endswith("/"):
        info("read_params", "Added missing '/' at the end of the -i/--input param")
        args.input += "/"

    # check that pcim_path is a valid directory
    if not os.path.isdir(args.input):
        error("read_params", "'"+args.input+"' is not a valid path or directory")

    # check that output ends with '/'
    if not args.output.endswith('/'):
        info("read_params", "Added missing '/' at the end of the -o/--output param")
        args.output += '/'

    # check that output is a valid directory
    if not os.path.isdir(args.output):
        error("read_params", "'"+args.output+"'is not a valid path or directory")

    return args


def read_inputs(input_folder):
    return iglob(input_folder+'*.expansion')


if __name__ == '__main__':
    args = read_params()
    expansions = read_inputs(args.input)
    probes_count = dict()
    probes_freq = dict()
    data_top = dict()
    data_thr = dict()

    for expansion in expansions:
        lst_top = []
        lst_thr = []
        idd = expansion[expansion.rfind('/')+1:expansion.rfind('.')]

        with open(expansion) as f:
            for row in f.readlines()[2:]:
                probe = row.strip().split(',')[1]
                freq_rel = float(row.strip().split(',')[3])

                if probe in probes_count:
                    probes_count[probe] += 1
                else:
                    probes_count[probe] = 1

                if probe in probes_freq:
                    probes_freq[probe].append(freq_rel)
                else:
                    probes_freq[probe] = [freq_rel]

                if len(lst_top) < args.top:
                    lst_top.append((probe, freq_rel))

                if freq_rel >= args.threshold:
                    lst_thr.append((probe, freq_rel))

                if (len(lst_top) == args.top) and (freq_rel < args.threshold):
                    break

        data_top[idd] = dict(lst_top)
        data_thr[idd] = dict(lst_thr)

    # MIN
    # output_filename = args.output+"top"+str(args.top)+"_min.tsv"
    output_filename = args.output+"top"+str(args.top)+"_min.expansion"

    if os.path.isfile(output_filename):
        output_filename_old = output_filename
        output_filename = args.output+"top"+str(args.top)+"_min."+str(time.time()).split('.')[0]+".tsv"
        info("main", '"'+output_filename_old+'" already exists, writing output to "'+output_filename+'"')

    with open(output_filename, 'w') as f:
        # f.write('\t'.join(['probe', 'norm', 'count', 'min.freqs', 'freqs.list']) + '\n')
        f.write(','.join(['pos', 'probe', 'count', 'norm', 'min.freqs']) + '\n')
        f.write(','.join(['pos', 'probe', 'count', 'norm', 'min.freqs']) + '\n')
        pos = 1

        for norm,probe in sorted(((v*min(probes_freq[k]), k) for k,v in probes_count.iteritems()), reverse=True):
            counts = str(probes_count[probe])
            min_freqs = str(min(probes_freq[probe]))
            freqs_list = ','.join([str(i) for i in probes_freq[probe]])

            # f.write('\t'.join([probe, str(norm), counts, min_freqs, freqs_list]) + '\n')
            f.write(','.join([str(pos), probe, counts, str(norm), min_freqs]) + '\n')
            pos += 1

    # MAX
    # output_filename = args.output+"top"+str(args.top)+"_max.tsv"
    output_filename = args.output+"top"+str(args.top)+"_max.expansion"

    if os.path.isfile(output_filename):
        output_filename_old = output_filename
        output_filename = args.output+"top"+str(args.top)+"_max."+str(time.time()).split('.')[0]+".tsv"
        info("main", '"'+output_filename_old+'" already exists, writing output to "'+output_filename+'"')

    with open(output_filename, 'w') as f:
        # f.write('\t'.join(['probe', 'norm', 'count', 'max.freqs', 'freqs.list']) + '\n')
        f.write(','.join(['pos', 'probe', 'count', 'norm', 'max.freqs']) + '\n')
        f.write(','.join(['pos', 'probe', 'count', 'norm', 'max.freqs']) + '\n')
        pos = 1

        for norm,probe in sorted(((v*max(probes_freq[k]), k) for k,v in probes_count.iteritems()), reverse=True):
            counts = str(probes_count[probe])
            max_freqs = str(max(probes_freq[probe]))
            freqs_list = ','.join([str(i) for i in probes_freq[probe]])

            # f.write('\t'.join([probe, str(norm), counts, max_freqs, freqs_list]) + '\n')
            f.write(','.join([str(pos), probe, counts, str(norm), max_freqs]) + '\n')
            pos += 1

    # MEAN
    # output_filename = args.output+"top"+str(args.top)+"_mean.tsv"
    output_filename = args.output+"top"+str(args.top)+"_mean.expansion"

    if os.path.isfile(output_filename):
        output_filename_old = output_filename
        output_filename = args.output+"top"+str(args.top)+"_mean."+str(time.time()).split('.')[0]+".tsv"
        info("main", '"'+output_filename_old+'" already exists, writing output to "'+output_filename+'"')

    with open(output_filename, 'w') as f:
        # f.write('\t'.join(['probe', 'norm', 'count', 'mean.freqs', 'freqs.list']) + '\n')
        f.write(','.join(['pos', 'probe', 'count', 'norm', 'mean.freqs']) + '\n')
        f.write(','.join(['pos', 'probe', 'count', 'norm', 'mean.freqs']) + '\n')
        pos = 1

        for norm,probe in sorted(((v*np.mean(probes_freq[k]), k) for k,v in probes_count.iteritems()), reverse=True):
            counts = str(probes_count[probe])
            mean_freqs = str(np.mean(probes_freq[probe]))
            freqs_list = ','.join([str(i) for i in probes_freq[probe]])

            # f.write('\t'.join([probe, str(norm), counts, mean_freqs, freqs_list]) + '\n')
            f.write(','.join([str(pos), probe, counts, str(norm), mean_freqs]) + '\n')
            pos += 1

    # MEDIAN
    # output_filename = args.output+"top"+str(args.top)+"_median.tsv"
    output_filename = args.output+"top"+str(args.top)+"_median.expansion"

    if os.path.isfile(output_filename):
        output_filename_old = output_filename
        output_filename = args.output+"top"+str(args.top)+"_median."+str(time.time()).split('.')[0]+".tsv"
        info("main", '"'+output_filename_old+'" already exists, writing output to "'+output_filename+'"')

    with open(output_filename, 'w') as f:
        # f.write('\t'.join(['probe', 'norm', 'count', 'median.freqs', 'freqs.list']) + '\n')
        f.write(','.join(['pos', 'probe', 'count', 'norm', 'median.freqs']) + '\n')
        f.write(','.join(['pos', 'probe', 'count', 'norm', 'median.freqs']) + '\n')
        pos = 1

        for norm,probe in sorted(((v*np.median(probes_freq[k]), k) for k,v in probes_count.iteritems()), reverse=True):
            counts = str(probes_count[probe])
            median_freqs = str(np.median(probes_freq[probe]))
            freqs_list = ','.join([str(i) for i in probes_freq[probe]])

            # f.write('\t'.join([probe, str(norm), counts, median_freqs, freqs_list]) + '\n')
            f.write(','.join([str(pos), probe, counts, str(norm), median_freqs]) + '\n')
            pos += 1
