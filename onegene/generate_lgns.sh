#!/bin/bash


for l in $(cut -f1 input/abundance_stoolsubset_speciesonly.txt | tail -n +2); do
	s=`echo ${l} | rev | cut -f1 -d'|' | rev`
	echo lgns/${s}.txt
	echo "from,to" > lgns/${s}.txt
	echo ${l},${l} >> lgns/${s}.txt
done
