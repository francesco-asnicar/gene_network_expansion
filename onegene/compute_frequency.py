#!/usr/bin/env python


from sys import argv
from glob import glob
from collections import Counter
import math


# tile_lst = glob('results.orig/*.csv')
tile_lst = glob('results/*.out')
counter = dict()
probe2id = dict()
iterations = int(argv[1])
lgn_file = argv[2]
obs_path = argv[3]
lgn = set()
dataset_size = None
tile_size = None
edges = []

with open(lgn_file) as f:
    for row in f.readlines()[1:]:
        lgn |= set(row.strip().split(','))

lgn = sorted(lgn)
print('lgn: {}'.format(lgn))

for tile in tile_lst:
    print('tile: {}'.format(tile))

    with open(tile) as f:
        for row in f:
            if row.startswith('#'):
                lst = row.strip().split(' ')[-1].split(',')

                # if not tile_size:
                #     tile_size = len(lst)

                for key in lst:
                    if key not in lgn:
                        if key in counter:
                            counter[key] += 1
                        else:
                            counter[key] = 1
            else:
                edges.append(','.join(sorted(row.strip().split(','))))

c = Counter(edges)

# with open('results/result_tiles', 'w') as f:
#     f.write('\n'.join(['\t'.join([k, str(c[k])]) for k in c]))

rowNumber = 0

with open(obs_path) as f:
    rows = f.readlines()[1:]

    # if not dataset_size:
    #     dataset_size = len(rows)

    for row in rows:
        # probeId = row.split('\t')[0] # TAB-separated
        probeId = row.split(',')[0] # COMMA-separated

        if probeId not in lgn:
            probe2id[probeId] = rowNumber

        rowNumber += 1

# offset = int(math.floor(float(dataset_size - len(lgn)) / float(tile_size - len(lgn))) * iterations)

# print "dataset_size", dataset_size
# print "len(lgn)", len(lgn)
# print "tile_size", tile_size
# print "iterations", iterations
# print "offset", offset

for k in counter.keys():
    counter[k] -= iterations

with open('frequency.txt', 'w') as f:
    for c in sorted(counter.keys()):
        if counter[c] > 0:
            f.write(str(probe2id[c]) + "\t" + str(counter[c]) + "\n")
