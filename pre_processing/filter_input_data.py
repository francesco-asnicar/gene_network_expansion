#!/usr/bin/env python

import os
from sys import stdout, stderr, exit
from argparse import ArgumentParser


def error(msg, fun=''):
    stderr.write("[e] ")
    if fun: stderr.write(fun+": ")
    stderr.write(msg+"\n")
    stderr.flush()


def info(msg, fun=''):
    stdout.write("[i] ")
    if fun: stdout.write(fun+": ")
    stdout.write(msg+"\n")
    stdout.flush()


def read_params():
    parser = ArgumentParser(description="")
    parser.add_argument('-i', '--input', type=str, required=True,
        help="Input data file to filter.")
    parser.add_argument('-o', '--output', type=str, required=False,
        help="Output filename, if not specified will be add \".out\" extension to the input filename.")
    parser.add_argument('-t', '--threshold', type=int, required=False, default=200,
        help="Threshold value of max missing values allowed, above which the row will be discarded.")
    parser.add_argument('-s', '--separator', type=str, required=False, default='\t',
        help="Specify the separator of the data. Default is the tab.")
    parser.add_argument('-n', '--null_value', type=str, required=False, default='',
        help="Specify the null value, default is the empty string.")
    args = parser.parse_args()

    # set output filename
    if not args.output:
        args.output = args.input+'.out'

    return args


if __name__ == '__main__':
    args = read_params()

    # check input file
    if not os.path.isfile(args.input):
        error("File \""+args.input+"\" not found")
        exit(1)

    # load the input file
    data = [r.replace('\n', '').split(args.separator) for r in open(args.input)]

    # filter it
    good_data = [data[0]]
    for r in data[1:]:
        nnull = len([e for e in r if e == args.null_value])

        if nnull < args.threshold:
            if nnull > 0:
                tmp = [float(e) for e in r[1:] if e != args.null_value]
                avg = str(sum(tmp) / len(tmp))
                new = []

                for e in r:
                    if e == args.null_value:
                        new.append(avg)
                    else:
                        new.append(e)

                good_data.append(new)
            else:
                good_data.append(r)

    # write the output file
    with open(args.output, 'w') as f:
        f.write('\n'.join([args.separator.join(r) for r in good_data]) + '\n')
