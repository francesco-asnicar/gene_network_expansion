#!/usr/bin/env python

import os
from sys import stdout, stderr, exit
from argparse import ArgumentParser


def error(msg, fun=''):
    stderr.write("[e] ")
    if fun: stderr.write(fun+": ")
    stderr.write(msg+"\n")
    stderr.flush()


def info(msg, fun=''):
    stdout.write("[i] ")
    if fun: stdout.write(fun+": ")
    stdout.write(msg+"\n")
    stdout.flush()


def read_params():
    parser = ArgumentParser(description="")
    parser.add_argument('-i', '--input', type=str, required=True,
        help="Input data file to filter.")
    parser.add_argument('-o', '--output', type=str, required=False,
        help="Output filename.")
    parser.add_argument('-r', '--rows', type=str, required=True,
        help="File with the list of rows ids to keep.")
    parser.add_argument('--separator', type=str, required=False, default='\t',
        help="Specify the separator of the data. Default is the tab.")
    args = parser.parse_args()

    # set output filename
    if not args.output:
        last_dot = args.input.rfind('.')
        args.output = args.input[:last_dot]+'_out'+args.input[last_dot+1:]

    return args


if __name__ == '__main__':
    args = read_params()

    # check input file
    if not os.path.isfile(args.input):
        error('File "'+args.input+'" not found')
        exit(1)

    # check input file
    if not os.path.isfile(args.rows):
        error('File "'+args.cols+'" not found')
        exit(1)

    # load the columns file
    rows = [row.strip() for row in open(args.rows)]

    # load the input file
    rows_ids = []
    data = []

    with open(args.input) as f:
        for row in f:
            if row.strip().split(args.separator)[0] in rows:
                data.append(row.strip().split(args.separator))

    # write output file
    with open(args.output, 'w') as f:
        f.write( '\n'.join( ['\t'.join(row) for row in data] ) + '\n')
