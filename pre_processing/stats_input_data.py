#!/usr/bin/env python

import os
from sys import stdout, stderr, exit
from argparse import ArgumentParser


def error(msg, fun=''):
    stderr.write("[e] ")
    if fun: stderr.write(fun+": ")
    stderr.write(msg+"\n")
    stderr.flush()


def info(msg, fun=''):
    stdout.write("[i] ")
    if fun: stdout.write(fun+": ")
    stdout.write(msg+"\n")
    stdout.flush()


def read_params():
    parser = ArgumentParser(description="")
    parser.add_argument('-i', '--input', type=str, required=True,
        help="Input data file to filter.")
    parser.add_argument('-o', '--output', type=str, required=False,
        help="Basename for output files, will be written _rows.out and _cols.out.")
    parser.add_argument('--skip_rows', type=str, required=False,
        help="Define the rows to skip in a csv manner, 0-indexed.")
    parser.add_argument('--separator', type=str, required=False, default='\t',
        help="Specify the separator of the data. Default is the tab.")
    parser.add_argument('-n', '--null_value', type=str, required=False, default='',
        help="Specify the null value, default is the empty string.")
    args = parser.parse_args()

    # set output filename
    if not args.output:
        args.output = args.input[:args.input.rfind('.')]

    # if there are rows to skip, make a list
    if args.skip_rows:
        args.skip_rows = [int(i) for i in args.skip_rows.strip().split(',')]
    else:
        args.skip_rows = []

    return args


if __name__ == '__main__':
    args = read_params()

    # check input file
    if not os.path.isfile(args.input):
        error("File \""+args.input+"\" not found")
        exit(1)

    # load the input file
    data_by_row = []
    rows_count = 0

    with open(args.input) as f:
        for row in f:
            if rows_count not in args.skip_rows:
                data_by_row.append(row.strip().split(args.separator))

            rows_count += 1

    # print data_by_row

    # write stat-by-row
    with open(args.output+'_stats_by_row.out', 'w') as f:
        for row in data_by_row:
            f.write('\t'.join([row[0], str(row.count(args.null_value))]) + '\n')
            # print '\t'.join([row[0], str(row.count(args.null_value))])

    data_by_col = []

    for col_id in xrange(len(data_by_row[0])):
        tmp_col = []

        for row in data_by_row:
            # print col_id
            tmp_col.append(row[col_id])

        data_by_col.append(tmp_col)

    # print data_by_col

    # write stat-by-col
    with open(args.output+'_stats_by_col.out', 'w') as f:
        for col in data_by_col:
            f.write('\t'.join([col[0], str(col.count(args.null_value))]) + '\n')
            # print '\t'.join([col[0], str(col.count(args.null_value))])
