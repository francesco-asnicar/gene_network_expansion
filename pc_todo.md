# PC to-do list
The tasks, ordered by priority, that should be developed in the stand-alone version of the PC++.

## HIGH


## MEDIUM
 - input file in different format (csv, tab, separator choose by the user, ..)


## LOW
 - più’ tiles?


## DONE
 - try to find the input format of some programs (e.g., R, Matlab) to print the graph, in order to provides the correspond output
 - implement the pc (a.k.a., direct the arcs!)
 - input parameters linux style (i.e., “-i” for input, “-o” for output, “-a” for the alpha value, “-s” for the separator, “-f” for the output format, ecc..)
 - find a solution that work with the “pcim.py” (problem in the output, pcim script defines a name while the pc++ would a directory!)
 - check memory leaks
 - separation set
 - decidere struttura sepset
 - change the “pc.cpp” & “pc.hpp” in “skeleton.cpp” & “skeleton.hpp”
 - check if it’s all ok